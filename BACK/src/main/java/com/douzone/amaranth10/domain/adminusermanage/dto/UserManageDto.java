package com.douzone.amaranth10.domain.adminusermanage.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class UserManageDto {
    private String id;
    private String password;
    private String userName;
    private int deptNo;
    private int rankNo;
    private String email;
    private boolean resign;
    private String deptName;
    private String rankName;
    private Integer offset;
}

package com.douzone.amaranth10.domain.Login.dao;

import com.douzone.amaranth10.domain.Login.dao.mapper.LoginMapper;
import com.douzone.amaranth10.domain.Login.dto.LoginInfoDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

@Repository
@Slf4j
@RequiredArgsConstructor
public class LoginDao {
    private final LoginMapper loginMapper;

    public String getPassword(String id){
        return loginMapper.getPassword(id);
    }
    public LoginInfoDto getLoginInfo(String id){
        return loginMapper.getLoginInfo(id);
    }
    public String checkGroupManager(String id) {
        return loginMapper.checkGroupManager(id);
    }
}

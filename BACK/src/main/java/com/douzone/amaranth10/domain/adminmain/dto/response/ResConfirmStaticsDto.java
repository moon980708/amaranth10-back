package com.douzone.amaranth10.domain.adminmain.dto.response;

import com.douzone.amaranth10.domain.adminmain.dto.response.MonthDeptDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResConfirmStaticsDto {

    private MonthDeptDto total;
    private List<MonthDeptDto> best5;
    private MonthDeptDto etc;

}
package com.douzone.amaranth10.domain.adminchargemanage.dto.response;

import lombok.Data;

@Data
public class ItemDto {
    private String itemTitle;
    private String itemContents;
}

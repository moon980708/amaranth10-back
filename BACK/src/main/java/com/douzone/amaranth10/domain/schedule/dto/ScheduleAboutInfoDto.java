package com.douzone.amaranth10.domain.schedule.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class ScheduleAboutInfoDto {
    private LocalDateTime prevChargeEnd = null;
    private LocalDateTime nextChargeStart = null;
    private LocalDateTime prevPayday = null;
    private LocalDateTime nextPayday = null;
}

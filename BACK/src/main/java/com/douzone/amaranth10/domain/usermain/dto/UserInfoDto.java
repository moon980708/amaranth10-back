package com.douzone.amaranth10.domain.usermain.dto;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
public class UserInfoDto {
    private String id;
//    private String password;
    private String userName;
    private String deptName;
    private String rankName;
    private String email;
}

package com.douzone.amaranth10.domain.creategroup.dto.request;

import lombok.Data;

@Data
public class ReqSearchGroupUserOptionDto {
    private int groupNo;
    private int page;
    private int rowsPerPage;

    private int start;   //limit에 사용될.
}

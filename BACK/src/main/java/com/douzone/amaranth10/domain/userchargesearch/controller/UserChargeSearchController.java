package com.douzone.amaranth10.domain.userchargesearch.controller;

import com.douzone.amaranth10.domain.usercharge.dto.UserChargeDto;
import com.douzone.amaranth10.domain.userchargesearch.application.UserChargeSearchService;
import com.douzone.amaranth10.domain.userchargesearch.dto.UserChargeSearchDto;
import com.douzone.amaranth10.domain.userchargesearch.dto.UserSearchOptionDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/chargelog")
@RequiredArgsConstructor
public class UserChargeSearchController {
    private final UserChargeSearchService userChargeSearchService;

    @GetMapping("/menuitem")
    public List<Integer> getYearMenuItem(@RequestParam String id){
        return userChargeSearchService.getYearMenuItem(id);
    }

    @PostMapping("/list")
    public List<UserChargeSearchDto> userChargeSearchList(
            @RequestBody UserSearchOptionDto userSearchOptionDto
    ) {
        return userChargeSearchService.userChargeSearchList(userSearchOptionDto);
    }

    @Value("${react.upload.folder}")
    private String uploadFolder; // React 폴더 경로

    @GetMapping("/{no}")
    public UserChargeSearchDto userChargeLogDetail(
            @PathVariable long no
    ) {
        UserChargeSearchDto userChargeSearchDto = userChargeSearchService.userChargeLogDetail(no);
        userChargeSearchDto.setFilePath(uploadFolder);
        return userChargeSearchDto;
    }

    @PutMapping("/recharge/{userChargeNo}")
    public void userRecharge(@PathVariable int userChargeNo){
        userChargeSearchService.userRecharge(userChargeNo);
    }
}

package com.douzone.amaranth10.domain.Login.dao.mapper;

import com.douzone.amaranth10.domain.Login.dto.LoginInfoDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;

@Mapper
public interface LoginMapper {
    public String getPassword(String id);
    public LoginInfoDto getLoginInfo(String id);
    public String checkGroupManager(String id);
}

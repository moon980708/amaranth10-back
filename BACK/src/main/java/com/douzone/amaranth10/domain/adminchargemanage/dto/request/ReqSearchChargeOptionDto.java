package com.douzone.amaranth10.domain.adminchargemanage.dto.request;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class ReqSearchChargeOptionDto {

    private String managerId;    //로그인id
    private int scheduleNo;
    private String state;
    private int groupDeptNo;
    private String id;

    private int page;
    private int rowsPerPage;

    private Timestamp searchTime;     //조회시간

    private int start;  //limit를 위한 start
}

package com.douzone.amaranth10.domain.matching.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class CategoryAboutItemListDto {
    private int itemNo;
    private String itemTitle;
    private String itemType;
    private Integer itemOption;
    private boolean itemEssential;
}

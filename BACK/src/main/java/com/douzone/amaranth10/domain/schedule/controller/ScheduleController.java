package com.douzone.amaranth10.domain.schedule.controller;


import com.douzone.amaranth10.domain.schedule.dto.*;
import com.douzone.amaranth10.domain.schedule.application.ScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class ScheduleController {

    private final ScheduleService scheduleService;

    @GetMapping("/scheduleload")
    public Integer scheduleLoad(String chargeDateString) {
        System.out.println("scheduleLoad");
        Integer scheduleNum = scheduleService.scheduleLoad(chargeDateString);
        if (scheduleNum == null) {
            return 1;
        } else {
            return scheduleNum;
        }

    }

    @GetMapping("/schedulechargeend")
    public HashMap<String, Object> scheduleChargeEnd(HashMap<String, Object> map, String chargeDateString, int createScheduleNum) {
        System.out.println("ScheduleController.scheduleChargeEnd");
        map.put("chargeDateString", chargeDateString);
        map.put("createScheduleNum", createScheduleNum);

        HashMap<String, Object> chargeEndAndPayday = scheduleService.scheduleChargeEnd(map);
        if (chargeEndAndPayday == null) {
            System.out.println("no Schedule");
            HashMap<String, Object> defaultResponse = new HashMap<>();
            defaultResponse.put("chargeEnd", chargeDateString.concat("-01"));

            //청구일을 다음 월의 1일로 설정해줌
            // Parse the date string into a LocalDate object
            LocalDate chargeDate = LocalDate.parse(chargeDateString.concat("-01"));
            // Add one month to the date
            LocalDate nextMonth = chargeDate.plusMonths(1);
            // Format the new date back into a string
            String nextMonthString = nextMonth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            defaultResponse.put("payday", nextMonthString);

            return defaultResponse;
        } else {
            HashMap<String, Object> response = new HashMap<>();
//            System.out.println("chargeEndAndPayday = " + chargeEndAndPayday);
            // Converting Date to String in yyyy-MM-dd format
            Timestamp chargeEndTimestamp = (Timestamp) chargeEndAndPayday.get("CHARGE_END");
            String chargeEnd = chargeEndTimestamp.toLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            response.put("chargeEnd", chargeEnd);

            Timestamp paydayTimestamp = (Timestamp) chargeEndAndPayday.get("PAYDAY");
            String payday = paydayTimestamp.toLocalDateTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            response.put("payday", payday);

            System.out.println("response = " + response);
            return response;

        }
    }

    @GetMapping("/schedulelist")
    public List<ScheduleListDto> yearMonthList(String chargeDateString) {
        List<ScheduleListDto> scheduleList = scheduleService.yearMonthList(chargeDateString);


        System.out.println("차수리스트 : " + scheduleList);
        return scheduleList;

    }


    @PutMapping("/schedule/updateclose/{scheduleNo}")
    public void updateScheduleClose(@RequestBody Map<String, Object> data, @PathVariable int scheduleNo) {
        String close = (String) data.get("close");

        HashMap<String, Object> map = new HashMap<>();
        map.put("scheduleNo", scheduleNo);
        map.put("close", close);

        scheduleService.updateScheduleClose(map);
        System.out.println("map=" + map);

    }

    @PostMapping("/schedule/createschedule")
    public void createNewSchedule(@RequestBody ScheduleInsertDto dto) {

        scheduleService.createNewSchedule(dto);
        System.out.println("dto=" + dto);


    }
    
    @DeleteMapping("/schedule/delete/{scheduleNo}")
    public void scheduleDelete(@PathVariable int scheduleNo){
        scheduleService.scheduleDelete(scheduleNo);
        System.out.println("scheduleNo = " + scheduleNo);
    }

    @DeleteMapping("/schedule/deleteAll")
    public void scheduleDeleteCheckAll(@RequestParam("selectScheduleNo") List<Integer> selectScheduleNo){
        scheduleService.scheduleDeleteCheckAll(selectScheduleNo);;
        System.out.println("checkList = " + selectScheduleNo);
    }

    @GetMapping("/schedule/modify/info")
    public List<ScheduleAboutInfoDto> getScheduleModifyAboutInfo(String yearMonth, Integer schedule){
        System.out.println("ScheduleController.getScheduleModifyAboutInfo");
        System.out.println("yearMonth = " + yearMonth);
        System.out.println("schedule = " + schedule);
        HashMap<String, Object> map = new HashMap<>();
        map.put("yearMonth", yearMonth);
        map.put("schedule", schedule);
        System.out.println("map = " + map);
//        HashMap<String, Object> result = scheduleService.getScheduleModifyAboutInfo(map);
//        System.out.println("result = " + result);
//예외처리 앞차수의 청구마감일이 ""일경우 , 뒷차수의 청구시작이 ""일경우
        List<ScheduleAboutInfoDto> newlist = scheduleService.getScheduleModifyAboutInfo(map);
        System.out.println("newlist = " + newlist);
//        System.out.println("newmap = " + newmap);
        if(newlist.get(0).getNextChargeStart() == null) {
            System.out.println(("NEXT_CHARGE_START는 null"));
            newlist.get(0).setNextChargeStart(null);
        }
        if(newlist.get(0).getPrevChargeEnd() == null) {
            System.out.println(("PREV_CHARGE_END는 null"));
            newlist.get(0).setPrevChargeEnd(null);
        }
        if(newlist.get(0).getPrevPayday() == null) {
            System.out.println(("PREV_PAYDAY는 null"));
            newlist.get(0).setPrevPayday(null);
        }
        if(newlist.get(0).getNextPayday() == null) {
            System.out.println(("NEXT_PAYDAY는 null"));
            newlist.get(0).setNextPayday(null);
        }

        System.out.println("마지막 newlist = " + newlist);
        return newlist;
    }
    @PutMapping("/schedule/modify")
    public void modifySchedule(@RequestBody ScheduleModifyDto dto){

        System.out.println("dto = " + dto);
        scheduleService.modifySchedule(dto);
    }

    @GetMapping("/isOngoingSchedule")
    public List<OngoingScheduleInfoDto> isOngoingSchedule(){
        System.out.println("ScheduleController.isOngoingSchedule");
        return scheduleService.isOngoingSchedule();
    }
}

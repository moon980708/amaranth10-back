package com.douzone.amaranth10.domain.matching.dao;

import com.douzone.amaranth10.domain.matching.dao.mapper.MatchingMapper;
import com.douzone.amaranth10.domain.matching.dto.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Slf4j
@RequiredArgsConstructor
public class MatchingDao {
    private final MatchingMapper matchingMapper;

    public List<CategoryListDto> getCategoryList(){
        return matchingMapper.getCategoryList();
    }
    public List<ItemListDto> getItemList(){
        return matchingMapper.getItemList();
    }

    public Integer checkMatchingItemList(HashMap<String, Object> map) {
        return matchingMapper.checkMatchingItemList(map);
    }
    public void insertMatchingItemList(HashMap<String,Object> map){
        matchingMapper.insertMatchingItemList(map);
    }
    public void updateMatchingItemList(HashMap<String,Object> map){
        matchingMapper.updateMatchingItemList(map);
    }
    public void deleteMatchingItemList(HashMap<String,Object> map){
        matchingMapper.deleteMatchingItemList(map);
    }

    public List<CategoryAboutItemListDto> getSelectCategoryAboutItemList(Integer categoryNo){
        return matchingMapper.getSelectCategoryAboutItemList(categoryNo);
    }
    public List<CategoryListDto> searchCategory(String keyword){
        return matchingMapper.searchCategory(keyword);
    }
    public List<ItemListDto> searchItem(String keyword){
        return matchingMapper.searchItem(keyword);
    }
}

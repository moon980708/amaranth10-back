package com.douzone.amaranth10.domain.adminmain.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnalysisOptionDto {
    private String managerId;
    private int groupDeptNo;
    private List<String> period;     //조회기간
    private String sort;    //정렬기준
}

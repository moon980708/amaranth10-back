package com.douzone.amaranth10.domain.Login.controller;

import com.douzone.amaranth10.domain.Login.application.LoginService;
import com.douzone.amaranth10.domain.Login.dto.InputLoginDto;
import com.douzone.amaranth10.domain.Login.dto.LoginInfoDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;

@RestController
@RequiredArgsConstructor

public class LoginController {

    private final LoginService loginService;

    @GetMapping("/login")
    public void loginTest(String id){
        System.out.println("LoginController.loginTest");
        loginService.encryptId(id);
    }
    @GetMapping("/email")
    public void emailTest(String email){
        System.out.println("LoginController.loginCheck");
        loginService.emailTest(email);

    }
    @PostMapping("/realLogin")
    public void realLogin(HttpSession session, @RequestBody @Valid InputLoginDto dto, HttpServletResponse response) throws IOException {
        System.out.println("LoginController.realLogin");
        String id = dto.getId();
        String password = dto.getPassword();

        if(loginService.realLogin(id,password).equals("loginFail")){
            response.sendRedirect("/sessionCheck");
        }
        else{
            //로그인 성공 시 세션 만들고 이동할 url을 리턴
            session.setAttribute("session", id);
            System.out.println("세션 생성 완료");
//            System.out.println("Session ID in realLogin: " + session.getId());
//            System.out.println("Session ID in loginTest: " + session.getId());
            response.sendRedirect("/sessionCheck");

        }

    }

    @RequestMapping("/sessionCheck")
    public String sessionCheck(HttpSession session){
            System.out.println("Session value: " + session.getAttribute("session"));

            if(session.getAttribute("session") == null){
                return "loginFail";
            }
            else if(loginService.checkGroupManager((String)session.getAttribute("session")) != null){
                return "admin/main";

            }
            else{
                return "user/userinfo";
            }

    }

    @PostMapping("/logout")
    public String logout(HttpSession session){
        session.invalidate();
        System.out.println("로그인 세션 무효화 완료");
        return "logout";
    }

    @GetMapping("/getLoginInfo")
    public LoginInfoDto getLoginInfo(HttpSession session){
        Object obj = session.getAttribute("session");
        System.out.println("LoginController.getLoginInfo");
        String id = (String) obj;
        System.out.println("id = " + id);
        LoginInfoDto dto = loginService.getLoginInfo(id);
        System.out.println("dto = " + dto);
        return dto;
    }

    @GetMapping("/loginCheck")
    public ResponseEntity<?> checkLoginStatus(HttpSession session){
        Object loginUser = session.getAttribute("session");

        if(loginUser == null){
            return ResponseEntity.ok().body("notLogin");
        }
        String loginUserString = (String)loginUser;
        if (loginUserString.equals("admin")){

            return ResponseEntity.ok().body("admin/main");
        }
        else{
            return ResponseEntity.ok().body("user/userinfo");
        }
    }



}

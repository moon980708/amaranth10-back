package com.douzone.amaranth10.domain.createcategoryanditem.application;

import com.douzone.amaranth10.domain.createcategoryanditem.dto.CategoryDto;
import com.douzone.amaranth10.domain.createcategoryanditem.dto.ItemDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.douzone.amaranth10.domain.createcategoryanditem.dao.CategoryDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryDao categoryDao;

    public List<CategoryDto> categorylist() {
        return categoryDao.categorylist();
    }

    public List<CategoryDto> findCategory(String categoryName) {   // 리스트 -> 용도 검색 기능
        return categoryDao.findCategory(categoryName);
    }

    public void insertCategory(CategoryDto categoryDto) {
        categoryDao.insertCategory(categoryDto);
    }

    public void matchingItemPrice(HashMap<String, Object> updateMap) { categoryDao.matchingItemPrice(updateMap); }

    public void updateCategory(HashMap<String, Object> updateMap) {
        categoryDao.updateCategory(updateMap);
    }

    @Transactional
    public void deleteCategory(List<Integer> checkedId) {
        for(int categoryNo : checkedId) {
            categoryDao.deleteCategory(categoryNo);
            categoryDao.deleteItemPrice(categoryNo);
        }
    }

    @Transactional
    public List<Integer> notDeleteCategory(List<Integer> checkedId) {
        List<Integer> notDeleteList = new ArrayList<>();

        for(int categoryNo : checkedId) {
            if((categoryDao.notDeleteCategory(categoryNo)).size() > 0) {   //기존 등록된 청구 내역 있는지 확인하는 쿼리 == "1"
                notDeleteList.add(categoryNo);
            }
        }
        return notDeleteList;
    }

    public List<ItemDto> itemlist() {
        return categoryDao.itemlist();
    }

    public List<ItemDto> findItem(String itemTitle) {
        return categoryDao.findItem(itemTitle);
    }

    public void insertItem(ItemDto itemDto) {
        categoryDao.insertItem(itemDto);
    }

//    public void updateItemDelete(HashMap<String, Object> updateMap) { categoryDao.updateItemDelete(updateMap); }

    @Transactional
    public void deleteItem(List<Integer> checkedId) {
        for(int itemNo : checkedId) {
//            HashMap<String, Object> updateMap = new HashMap<>();
//            updateMap.put("categoryNo", categoryNo);
//            updateMap.put("categoryDelete", true);

            categoryDao.deleteItem(itemNo);
            categoryDao.deleteItemOption(itemNo);
        }
    }

    @Transactional
    public List<Integer> notDeleteItem(List<Integer> checkedId) {
        List<Integer> notDeleteList = new ArrayList<>();

        for(int itemNo : checkedId) {
            if((categoryDao.notDeleteItem(itemNo)).size() > 0) {   //기존 등록된 청구 내역 있는지 확인하는 쿼리 == "1"
                notDeleteList.add(itemNo);
            }
        }
        return notDeleteList;
    }

//    public List<CategoryDto> categorylistByCategoryNo(int categoryNo) {
//        return categoryDao.categorylistByCategoryNo(categoryNo);
//    }
}
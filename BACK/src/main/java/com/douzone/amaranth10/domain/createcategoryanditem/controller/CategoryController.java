package com.douzone.amaranth10.domain.createcategoryanditem.controller;

import com.douzone.amaranth10.domain.createcategoryanditem.application.CategoryService;
import com.douzone.amaranth10.domain.createcategoryanditem.dto.CategoryDto;
import com.douzone.amaranth10.domain.createcategoryanditem.dto.ItemDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/admin/categorylist")
    public List<CategoryDto> categorylist(@RequestParam(required = false) String categoryName) {
        if (categoryName != null && !categoryName.isEmpty()) {
            // 검색어가 있을 경우, 해당 검색어로 데이터베이스에서 검색하고 결과를 반환
            return categoryService.findCategory(categoryName);
        } else {
            // 검색어가 없을 경우, 전체 용도 리스트 반환
            return categoryService.categorylist();
        }
    }

    @PostMapping("/admin/category/add")   // 용도 추가
    public void insertCategory(@RequestBody CategoryDto categoryDto) {
        categoryService.insertCategory(categoryDto);   // 용도 insert

        HashMap<String, Object> updateMap = new HashMap<>();   // 100번 항목 insert
        updateMap.put("categoryNo", categoryDto.getCategoryNo());
        updateMap.put("itemNo", 100);

        categoryService.matchingItemPrice(updateMap);
    }

    @PutMapping("/admin/category/update/{categoryNo}")   // 용도 사용여부 수정
    public void updateCategory(@RequestBody HashMap<String, Object> map, @PathVariable int categoryNo) {
        boolean usable = (boolean) map.get("usable");

        HashMap<String, Object> updateMap = new HashMap<>();
        updateMap.put("categoryNo", categoryNo);
        updateMap.put("usable", !usable);

        categoryService.updateCategory(updateMap);
    }

    @DeleteMapping ("/admin/category/delete")
    public void deleteCategory(@RequestParam("checkedId") List<Integer> checkedId) {
        categoryService.deleteCategory(checkedId);
    }

    @GetMapping("/admin/category/notDeleteList")
    public List<Integer> notDeleteCategory(@RequestParam("checkedId") List<Integer> checkedId) {
        List<Integer> notDeleteList = categoryService.notDeleteCategory(checkedId);

        return notDeleteList;
    }

    @GetMapping("/admin/itemlist")
    public List<ItemDto> itemlist(@RequestParam(required = false) String itemTitle) {
        if (itemTitle != null && !itemTitle.isEmpty()) {
            return categoryService.findItem(itemTitle);
        } else {
            return categoryService.itemlist();
        }
    }

    @PostMapping("/admin/item/add")   // 항목 추가
    public void insertItem(@RequestBody ItemDto itemDto) {
        categoryService.insertItem(itemDto);
    }

    @DeleteMapping ("/admin/item/delete")
    public void deleteItem(@RequestParam("checkedId") List<Integer> checkedId) {
        categoryService.deleteItem(checkedId);
    }

    @GetMapping("/admin/item/notDeleteList")
    public List<Integer> notDeleteItem(@RequestParam("checkedId") List<Integer> checkedId) {
        List<Integer> notDeleteList = categoryService.notDeleteItem(checkedId);

        return notDeleteList;
    }
}
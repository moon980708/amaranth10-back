package com.douzone.amaranth10.domain.usermain.dto;

import lombok.Data;

@Data
public class UserUpdateEmailDto {
    private String id;
    private String email;
}

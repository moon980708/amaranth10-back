package com.douzone.amaranth10.domain.adminmain.dto.response;

import com.douzone.amaranth10.domain.entity.Dept;
import com.douzone.amaranth10.domain.entity.Group;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResGroupDeptDto {
    private List<Group> group;
    private List<Dept> dept;
}

package com.douzone.amaranth10.domain.adminmain.dto.response;

import lombok.Data;

import java.math.BigInteger;

@Data
public class ResWaitDto {
    private int totalCount;
    private long totalSum;
}

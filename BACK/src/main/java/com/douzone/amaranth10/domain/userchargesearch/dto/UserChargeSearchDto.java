package com.douzone.amaranth10.domain.userchargesearch.dto;

import com.douzone.amaranth10.domain.usercharge.dto.UserChargeDetailAddDto;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class UserChargeSearchDto {
    private long userChargeNo;
    private int categoryNo;
    private String categoryName;
    private int schedule;
    private int scheduleNo;
    private String expendDate;
    private String store;
    private long charge;
    private String contents;
    private String payment;
    private String userChargeDate;
    private String state;
    private boolean rejectCheck;
    private String fileName;
    private String realFileName;
    private int itemNo;
    private String filePath;
    private String rejectReason;
    private List<UserChargeDetailDto> itemContents;
}

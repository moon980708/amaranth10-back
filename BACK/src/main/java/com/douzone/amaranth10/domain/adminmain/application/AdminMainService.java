package com.douzone.amaranth10.domain.adminmain.application;

import com.douzone.amaranth10.domain.adminmain.dto.request.ReqMonthDeptDto;
import com.douzone.amaranth10.domain.adminmain.dto.request.ReqMonthScheduleDto;
import com.douzone.amaranth10.domain.adminmain.dao.AdminMainDao;
import com.douzone.amaranth10.domain.adminmain.dto.response.*;
import com.douzone.amaranth10.domain.adminmain.dto.request.AnalysisOptionDto;
import com.douzone.amaranth10.domain.adminmain.dto.request.ReqAnalysisOptionDto;
import com.douzone.amaranth10.domain.entity.Dept;
import com.douzone.amaranth10.domain.entity.Group;
import com.douzone.amaranth10.domain.entity.Schedule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AdminMainService {
    private final AdminMainDao adminMainDao;

    public ResWaitDto listWait(String managerId) {
        return adminMainDao.listWait(managerId);
    }

    public ResConfirmStaticsDto getDeptConfirmByMonth(ReqMonthDeptDto dto) {
        MonthDeptDto total = adminMainDao.totalMonthConfirm(dto);   //청구년월에 해당하는 내역중 승인의 전체 합계/건수
        log.info(total.toString());
        List<MonthDeptDto> best5 = adminMainDao.listMonthDept5(dto);   //1등~5등 부서
        return ResConfirmStaticsDto.builder()
                .total(total)
                .best5(best5)
                .etc(getEtc(total, best5))
                .build();
    }

    public List<Schedule> listMonthSchedule(ReqMonthScheduleDto dto) {
        return adminMainDao.listMonthSchedule(dto);
    }

    public ResGroupDeptDto listGroupDept(String managerId) {
        List<Group> usableGroup = adminMainDao.listUsableGroup(managerId);
        List<Dept> deptList = null;

        if(managerId.equals("admin")) {
            deptList = adminMainDao.listDept();
        }

        return ResGroupDeptDto.builder()
                .group(usableGroup)
                .dept(deptList)
                .build();
    }

    public ResAnalysisByGroupDeptByPeriodDto listAnalysis(ReqAnalysisOptionDto dto) {
        BaseAnalysisDto totalConfirm = adminMainDao.totalGroupDeptByPeriod(dto);
        BaseAnalysisDto mostCountDept = adminMainDao.mostCountDeptByPeriod(dto);

        List<BaseAnalysisDto> categoryList = adminMainDao.listCategoryByPeriod(dto);
        //용도별(총 금액, 건수) -> front에서 금액순/건수순 으로 정렬

        List<MonthlyChargeDto> monthlyApplyCharge = adminMainDao.monthlyApplyCharge(dto);
        List<MonthlyChargeDto> monthlyConfirmCharge = adminMainDao.monthlyConfirmCharge(dto);

        List<UserChargeRankDto> userRankBySum = adminMainDao.listUserChargeRankByPeriod(getAddSortOption(dto,"sum"));
        List<UserChargeRankDto> userRankByCount = adminMainDao.listUserChargeRankByPeriod(getAddSortOption(dto,"count"));



        return ResAnalysisByGroupDeptByPeriodDto.builder()
                .totalConfirm(totalConfirm)
                .mostCountDept(mostCountDept)
                .categoryList(categoryList)
                .monthlyApplyCharge(getMonthlyCharge(monthlyApplyCharge, dto))
                .monthlyConfirmCharge(getMonthlyCharge(monthlyConfirmCharge, dto))
                .userRankBySum(userRankBySum)
                .userRankByCount(userRankByCount)
                .build();
    }


    // 권한 체크
    public String getUserResignCheck(String id) {
        return adminMainDao.getUserResignCheck(id);
    }


    public String getGroupManagerCheck(String id) {
        return adminMainDao.getGroupManagerCheck(id);
    }







    //private method
    private MonthDeptDto getEtc(MonthDeptDto total, List<MonthDeptDto> best5) {
        if(best5 == null) return null;

        int etcCount = total.getCountChargeDept();    //처음엔 total로 셋팅
        BigInteger etcSum = total.getSumChargeDept();

        // 1등~5등 부서의 건수와 합계를 뺌
        for (MonthDeptDto dept : best5) {
            etcCount -= dept.getCountChargeDept();
            etcSum = etcSum.subtract(dept.getSumChargeDept());
        }

        if(etcCount == 0) return null;

        return MonthDeptDto.builder()
                .deptName("그 외")
                .countChargeDept(etcCount)
                .sumChargeDept(etcSum)
                .build();
    }

    private List<MonthlyChargeDto> getMonthlyCharge(List<MonthlyChargeDto> queryResult, ReqAnalysisOptionDto reqDto) {
        List<String> period = reqDto.getPeriod();   // ArrayList로 변환  ["2023-06","2023-07","2023-08"]

        if(period.size() == queryResult.size()) {
            return queryResult;
        }

        List<MonthlyChargeDto> result = new ArrayList<>(); // result 리스트 초기화

        for(String chargeMonth : period) {
            boolean found = false;     //해당 월을 찾았는지
            for(MonthlyChargeDto dto : queryResult) {
                if(dto.getChargeMonth().equals(chargeMonth)) {
                    result.add(dto);
                    found = true;
                    break;
                }
            }

            if (!found) {    //못찾았으면 건수와 합계 0으로 초기화하고 새로 만들어서 넣기
                result.add(MonthlyChargeDto.builder()
                                .chargeMonth(chargeMonth)
                                .count(0)
                                .sum(0)
                                .build());
            }
        }

        return result;
    }

    private AnalysisOptionDto getAddSortOption(ReqAnalysisOptionDto dto, String sort) {
        return AnalysisOptionDto.builder()
                .managerId(dto.getManagerId())
                .groupDeptNo(dto.getGroupDeptNo())
                .period(dto.getPeriod())
                .sort(sort)
                .build();
    }


}

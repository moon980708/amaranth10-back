package com.douzone.amaranth10.domain.schedule.dao.mapper;

import com.douzone.amaranth10.domain.schedule.dto.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface ScheduleMapper {

    public Integer scheduleLoad(String chargeDateString);

    public HashMap<String, Object> scheduleChargeEnd(HashMap<String, Object> map);

    public List<ScheduleListDto> yearMonthList(String chargeDateString);

    public void updateScheduleClose(HashMap<String, Object> map);

    public void createNewSchedule(ScheduleInsertDto dto);

    public void scheduleDelete(int scheduleNo);

    public List<ScheduleAboutInfoDto> getScheduleModifyAboutInfo(HashMap<String,Object> map);
    public void updateCloseToCloseScheduler(Integer scheduleNo);
    public void updateCloseToBeginScheduler(Integer scheduleNo);
    public void modifySchedule(ScheduleModifyDto dto);
    public List<OngoingScheduleInfoDto> isOngoingSchedule();

    public void deleteRegisterCharge(Integer scheduleNo);


}

package com.douzone.amaranth10.domain.usercharge.application;


import com.douzone.amaranth10.domain.usercharge.dao.UserChargeDao;
import com.douzone.amaranth10.domain.usercharge.dto.UserCategoryDto;
import com.douzone.amaranth10.domain.usercharge.dto.UserChargeAddDto;
import com.douzone.amaranth10.domain.usercharge.dto.UserChargeDto;
import com.douzone.amaranth10.domain.usercharge.dto.UserScheduleViewDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class UserChargeService {

    private final UserChargeDao userChargeDao;

    public List<UserScheduleViewDto> userChargeSchedule() {
        List<UserScheduleViewDto> userChargeSchedule = userChargeDao.userChargeSchedule();
        return userChargeSchedule;
    }

    public List<UserChargeDto> userChargeList(String id, String searchValue, String searchField) {
        List<UserChargeDto> userChargeList = userChargeDao.userChargeList(id, searchValue, searchField);
        return userChargeList;
    }

    public UserChargeDto userChargeDetail(long no) {
        UserChargeDto userChargeDto = userChargeDao.userChargeDetail(no);
        return userChargeDto;
    }

    public List<Map<String, List<String>>> userChargeCategoryDetail(long userChargeNo) {
        return userChargeDao.userChargeCategoryDetail(userChargeNo);
    }

    public List<Map<String, List<String>>> categoryDetailForm(int categoryNo) {
        return userChargeDao.categoryDetailForm(categoryNo);

    }

    public List<UserCategoryDto> userCategories() {
        List<UserCategoryDto> userCategoryDto = userChargeDao.userCategories();
        return userCategoryDto;
    }

    @Transactional
    public String userChargeUpdate(List<Long> userChargeNo) {
        return userChargeDao.userChargeUpdate(userChargeNo);
    }

    @Transactional
    public void userChargeDelete(List<Long> userChargeNo) {
        userChargeDao.userChargeDelete(userChargeNo);
    }

    public void saveChargeData(UserChargeAddDto userChargeAddDto) {
        userChargeDao.saveChargeData(userChargeAddDto);
    }


//    public long checkDuplicate(String id, String expendDate, long userChargeNo) {
//        long duplicateCount = userChargeDao.checkDuplicate(id, expendDate, userChargeNo);
//        return duplicateCount;
//    }

    public void modifyChargeData(long userchargeno, UserChargeAddDto userChargeAddDto) {
        userChargeDao.modifyChargeData(userchargeno,userChargeAddDto);

    }

    public void updateRealFileName(String newFileName, long userChargeNo) {
        userChargeDao.updateRealFileName(newFileName,userChargeNo);
    }

    public void saveRealFileName(String newFileName, long charge, String id, String expendDate, int categoryNo) {
        userChargeDao.saveRealFileName(newFileName, charge, id, expendDate, categoryNo);
    }

    public String ongoingSchedule() {
        return userChargeDao.ongoingSchedule();
    }
}

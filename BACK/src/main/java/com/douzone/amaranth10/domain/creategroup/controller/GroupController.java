package com.douzone.amaranth10.domain.creategroup.controller;

import com.douzone.amaranth10.domain.creategroup.application.GroupService;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqGroupAndManagerDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqSearchGroupUserOptionDto;
import com.douzone.amaranth10.domain.creategroup.dto.response.ResDeptUserDto;
import com.douzone.amaranth10.domain.creategroup.dto.response.ResGroupDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqGroupAndMemberDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqSearchGroupOptionDto;
import com.douzone.amaranth10.domain.entity.Group;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/group")
public class GroupController {
    private final GroupService groupService;

    @PostMapping("/list")
    public List<ResGroupDto> listGroup(@RequestBody ReqSearchGroupOptionDto dto) {
        System.out.println(dto);
        return groupService.listGroup(dto);
    }

    @PostMapping("/member/list")
    public List<ResDeptUserDto> listGroupMember(@RequestBody ReqSearchGroupUserOptionDto dto) {
        System.out.println("그룹멤버리스트 불러올거야: " + dto);
        return groupService.listGroupMember(dto);
    }

    @PostMapping("/manager/list")
    public List<ResDeptUserDto> listGroupManager(@RequestBody ReqSearchGroupUserOptionDto dto) {
        System.out.println("그룹담당자리스트 불러올거야: " + dto);
        return groupService.listGroupManager(dto);
    }

    @GetMapping("/dept/userlist")
    public List<ResDeptUserDto> listDeptUser() {
        System.out.println("조직도 불러오기");
        return groupService.listDeptUser();
    }

    @PostMapping("/add")
    public int insertGroup(@RequestBody ReqGroupAndMemberDto dto) {
        System.out.println("그룹생성할거얌"+ dto);
        //생성한 그룹no 반환
        return groupService.insertGroup(dto);
    }

    @PutMapping("/member/update")
    public void editGroupMember(@RequestBody ReqGroupAndMemberDto dto) {
        System.out.println("수정할 그룹정보(멤버):"+dto);
        //지우고 나서 새로 넣어주기 -> 수정
        groupService.editGroupMember(dto);
    }

    @DeleteMapping("/member/delete")
    public void deleteGroupMember(@RequestBody ReqGroupAndMemberDto dto) {
        System.out.println("지우고싶은 그룹정보(멤버):"+dto);
        groupService.deleteGroupMember(dto);
    }

    @PutMapping("/manager/update")
    public void editGroupManager(@RequestBody ReqGroupAndManagerDto dto) {
        System.out.println("수정할 그룹정보(담당자):"+dto);
        //지우고 나서 새로 넣어주기 -> 수정
        groupService.editGroupManager(dto);
    }

    @DeleteMapping("/manager/delete")
    public void deleteGroupManager(@RequestBody ReqGroupAndManagerDto dto) {
        System.out.println("지우고싶은 그룹정보(담당자):"+dto);
        groupService.deleteGroupManager(dto);
    }

    @PutMapping("/update")
    public void editGroup(@RequestBody Group dto) {
        System.out.println("수정할 그룹정보: "+dto);
        groupService.editGroup(dto);
    }

    @DeleteMapping("/delete/{groupNo}")
    public void deleteGroup(@PathVariable int groupNo) {
        System.out.println("삭제할고얌");
        System.out.println("삭제하려는 그룹번호: "+groupNo);
        groupService.deleteGroup(groupNo);
    }




}

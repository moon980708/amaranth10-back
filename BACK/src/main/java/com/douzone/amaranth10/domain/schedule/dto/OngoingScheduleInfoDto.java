package com.douzone.amaranth10.domain.schedule.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class OngoingScheduleInfoDto {
    private LocalDateTime yearMonth;
    private int schedule;
}

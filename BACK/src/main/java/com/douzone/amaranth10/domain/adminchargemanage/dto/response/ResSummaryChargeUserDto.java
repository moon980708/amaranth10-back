package com.douzone.amaranth10.domain.adminchargemanage.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResSummaryChargeUserDto {
    private ChargeUserDto total;
    private List<ChargeUserDto> user;

}

package com.douzone.amaranth10.domain.userchargesearch.dao.mapper;

import com.douzone.amaranth10.domain.userchargesearch.dto.UserChargeSearchDto;
import com.douzone.amaranth10.domain.userchargesearch.dto.UserSearchOptionDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserChargeSearchMapper {
    List<UserChargeSearchDto> userChargeSearchList(
           @Param("userSearchOptionDto") UserSearchOptionDto userSearchOptionDto
    );

    List<Integer> getYearMenuItem(@Param("id") String id);

    UserChargeSearchDto userChargeLogDetail( @Param("no") long no);

    void userRecharge(@Param("userChargeNo") int userChargeNo);
}

package com.douzone.amaranth10.domain.adminmain.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class ReqAnalysisOptionDto {
    private String managerId;
    private int groupDeptNo;
    private List<String> period;     //조회기간
}

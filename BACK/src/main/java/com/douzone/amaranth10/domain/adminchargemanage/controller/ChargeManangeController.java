package com.douzone.amaranth10.domain.adminchargemanage.controller;

import com.douzone.amaranth10.domain.adminchargemanage.application.ChargeManageService;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqSearchChargeOptionDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqSearchChargeUserOptionDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqUpdateStateDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.response.ResChargeDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.response.ResSummaryChargeUserDto;
import com.douzone.amaranth10.domain.entity.Schedule;
import com.douzone.amaranth10.global.response.CommonResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/charge")
public class ChargeManangeController {
    private final ChargeManageService chargeManageService;

    @GetMapping("/schedulelist")
    public List<Schedule> listSchedule() {
        return chargeManageService.listSchedule();
    }

    @PostMapping("/search")
    public ResSummaryChargeUserDto summaryChargeUser(@RequestBody ReqSearchChargeUserOptionDto dto) {

        return chargeManageService.summaryChargeUser(dto);
    }

    @PostMapping("/list")
    public List<ResChargeDto> listCharge(@RequestBody ReqSearchChargeOptionDto dto) {
        return chargeManageService.listCharge(dto);
    }

    @GetMapping("/detail/{userChargeNo}")
    public ResChargeDto getChargeDetail(@PathVariable int userChargeNo) {
        return chargeManageService.getChargeDetail(userChargeNo);
    }

    @PutMapping("/update")
    public ResponseEntity<CommonResponse<String>> updateStateUserCharge(@RequestBody ReqUpdateStateDto dto) {
        System.out.println(dto);
        String message = chargeManageService.updateStateUserCharge(dto);

        return ResponseEntity.status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(CommonResponse.successWith(message));
    }


}

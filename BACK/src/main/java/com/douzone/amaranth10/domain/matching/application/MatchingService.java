package com.douzone.amaranth10.domain.matching.application;

import com.douzone.amaranth10.domain.matching.dao.MatchingDao;
import com.douzone.amaranth10.domain.matching.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MatchingService {

    private final MatchingDao matchingDao;

    public List<CategoryListDto> getCategoryList(){
        return matchingDao.getCategoryList();
    }
    public List<ItemListDto> getItemList(){
        return matchingDao.getItemList();
    }

    @Transactional
    public void checkMatchingItemList(Integer categoryNo, List<HashMap<String, Object>> dataList){

        List<CategoryAboutItemListDto> existMatchingList = matchingDao.getSelectCategoryAboutItemList(categoryNo);

        for(HashMap<String, Object> map : dataList){
            if(matchingDao.checkMatchingItemList(map) != null){
                System.out.println("update하는거 : " + map);
                matchingDao.updateMatchingItemList(map);
            }
            else{
                System.out.println("insert하는거 : " + map);
                matchingDao.insertMatchingItemList(map);
            }
        }

        for(CategoryAboutItemListDto existingData : existMatchingList){
            boolean existInDataList = dataList.stream().anyMatch(
                    data -> data.get("itemNoList").equals(existingData.getItemNo())
            );

            if(!existInDataList){
                HashMap<String,Object> deleteMap = new HashMap<>();
                deleteMap.put("categoryNo", categoryNo);
                deleteMap.put("itemNoList", existingData.getItemNo());
                System.out.println("delete하는거 : " + deleteMap);
                matchingDao.deleteMatchingItemList(deleteMap);
            }
        }

    }
    public List<CategoryAboutItemListDto> getSelectCategoryAboutItemList(Integer categoryNo){
        return matchingDao.getSelectCategoryAboutItemList(categoryNo);
    }
    public List<CategoryListDto> searchCategory(String keyword){
        return matchingDao.searchCategory(keyword);
    }
    public List<ItemListDto> searchItem(String keyword){
        return matchingDao.searchItem(keyword);
    }

}

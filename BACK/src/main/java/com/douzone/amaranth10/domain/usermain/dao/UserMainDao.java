package com.douzone.amaranth10.domain.usermain.dao;

import com.douzone.amaranth10.domain.usermain.dao.mapper.UserMainMapper;
import com.douzone.amaranth10.domain.usermain.dto.*;
import com.douzone.amaranth10.domain.usermain.dto.UserInfoDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
@Slf4j
@RequiredArgsConstructor
public class UserMainDao {
    private final UserMainMapper userMainMapper;


    public UserInfoDto userInfo(String id) {
        return userMainMapper.userInfo(id);
    }

    public void userInfoUpdate(UserInfoModifyDto userInfoModifyDto) {
        userMainMapper.userInfoUpdate(userInfoModifyDto);
    }

    public List<ChargeSummaryDto> userChargeSummary(String id) {
        return userMainMapper.userChargeSummary(id);
    }

    public List<CategoryChartDto> userCategoryChart(String id, int year, int month) {
        return userMainMapper.userCategoryChart(id, year, month);
    }

    public List<MonthlyChartDto> userMonthlyChart(String id) {
        return userMainMapper.userMonthlyChart(id);
    }
    public String getPassword(String id){
        return  userMainMapper.getPassword(id);
    }
    public void userInfoEmailUpdate(UserUpdateEmailDto emailDto){
        userMainMapper.userInfoEmailUpdate(emailDto);
    }
    public void userInfoPasswordUpdate(UserUpdatePasswordDto passwordDto){
        userMainMapper.userInfoPasswordUpdate(passwordDto);
    }
}

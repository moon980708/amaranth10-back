package com.douzone.amaranth10.domain.userchargesearch.application;

import com.douzone.amaranth10.domain.usercharge.dto.UserChargeDto;
import com.douzone.amaranth10.domain.userchargesearch.dao.UserChargeSearchDao;
import com.douzone.amaranth10.domain.userchargesearch.dto.UserChargeSearchDto;
import com.douzone.amaranth10.domain.userchargesearch.dto.UserSearchOptionDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserChargeSearchService {
    private final UserChargeSearchDao userChargeSearchDao;

    public List<UserChargeSearchDto> userChargeSearchList(UserSearchOptionDto userSearchOptionDto) {
        List<UserChargeSearchDto> userChargeSearchList = userChargeSearchDao.userChargeSearchList(userSearchOptionDto);
        return userChargeSearchList;

    }

    public List<Integer> getYearMenuItem(String id) {
        List<Integer> getYearMenuItem = userChargeSearchDao.getYearMenuItem(id);
        return getYearMenuItem;
    }

    public UserChargeSearchDto userChargeLogDetail(long no) {
        UserChargeSearchDto userChargeSearchDto = userChargeSearchDao.userChargeLogDetail(no);
        return userChargeSearchDto;
    }

    public void userRecharge(int userChargeNo) {
        userChargeSearchDao.userRecharge(userChargeNo);
    }
}

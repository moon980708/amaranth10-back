package com.douzone.amaranth10.domain.usercharge.controller;

import com.douzone.amaranth10.domain.usercharge.application.UserChargeService;
import com.douzone.amaranth10.domain.usercharge.dto.UserCategoryDto;
import com.douzone.amaranth10.domain.usercharge.dto.UserChargeAddDto;
import com.douzone.amaranth10.domain.usercharge.dto.UserChargeDto;
import com.douzone.amaranth10.domain.usercharge.dto.UserScheduleViewDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user/charge")
@RequiredArgsConstructor
public class UserChargeController {
    private final UserChargeService userChargeService;

    @GetMapping("/schedule")
    public List<UserScheduleViewDto> userChargeSchedule() {
        return userChargeService.userChargeSchedule();
    }

    @GetMapping("/ongoingschedule")
    public String ongoingSchedule() {return userChargeService.ongoingSchedule();};

    @GetMapping("/list")
    public List<UserChargeDto> userChargeList( //조회 및 검색
                                               @RequestParam String id,
                                               @RequestParam("searchValue") String searchValue,
                                               @RequestParam("searchField") String searchField
    ) {
        return userChargeService.userChargeList(id, searchValue, searchField);
    }

    @GetMapping("/{no}")
    public UserChargeDto userChargeDetail(
            @PathVariable long no
    ) {
        UserChargeDto userChargeDto = userChargeService.userChargeDetail(no);
        userChargeDto.setFilePath(uploadFolder);
        return userChargeDto;
    }

    @GetMapping("/categorydetail")
    public List<Object> userChargeCategoryDetail(
            @RequestParam long userChargeNo
    ) {
        List<Object> list = new ArrayList<>();
        List<Map<String, List<String>>> resultList = userChargeService.userChargeCategoryDetail(userChargeNo);
        list.add(resultList);
        return list;
    }

    @GetMapping("/categorydetailform")
    public List<Object> categoryDetailForm(
            @RequestParam int categoryNo
    ) {
        List<Object> list = new ArrayList<>();
        List<Map<String, List<String>>> resultList = userChargeService.categoryDetailForm(categoryNo);
        list.add(resultList);
        return list;
    }

    @GetMapping("/categories")
    public List<UserCategoryDto> userCategories() {
        return userChargeService.userCategories();
    }

    @PutMapping("/update")
    public String userChargeUpdate(
            @RequestBody List<Long> userChargeNo
    ) {
        return userChargeService.userChargeUpdate(userChargeNo);
    }

    @DeleteMapping("/delete")
    public void userChargeDelete(
            @RequestBody List<Long> userChargeNo
    ) {
        userChargeService.userChargeDelete(userChargeNo);
    }

    @PostMapping("/save")
    public String saveChargeData(
            @RequestBody UserChargeAddDto userChargeAddDto
    ) {
        userChargeService.saveChargeData(userChargeAddDto);
        return userChargeAddDto.getFileName();
    }

    @Value("${react.upload.folder}")
    private String uploadFolder; // React 폴더 경로

    @PostMapping("/fileupdate")
    public String updateFile(
            @RequestPart("file") MultipartFile file,
            @RequestParam("userChargeNo") long userChargeNo
    ) {
        try {
            int r = (int)(Math.random()*1000000); //랜덤 난수
            long t = System.currentTimeMillis(); //오늘 날짜 밀리초
            String originalFilename = file.getOriginalFilename(); // 파일 이름
            int dotIndex = originalFilename.lastIndexOf('.'); // 확장자 구분 점 위치
            String extension = originalFilename.substring(dotIndex); // 확장자
            String newFileName = t+r+extension;
            File targetFile = new File(uploadFolder, newFileName);
            file.transferTo(targetFile);
            userChargeService.updateRealFileName(newFileName, userChargeNo);
            return newFileName;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @PostMapping("/fileupload")
    public String uploadFile(
            @RequestPart("file") MultipartFile file,
            @RequestParam("charge") long charge,
            @RequestParam("id") String id,
            @RequestParam("expendDate") String expendDate,
            @RequestParam("categoryNo") int categoryNo
    ) {
        try {
            int r = (int)(Math.random()*1000000); //랜덤 난수
            long t = System.currentTimeMillis(); //오늘 날짜 밀리초
            String originalFilename = file.getOriginalFilename(); // 파일 이름
            int dotIndex = originalFilename.lastIndexOf('.'); // 확장자 구분 점 위치
            String extension = originalFilename.substring(dotIndex); // 확장자
            String newFileName = t+r+extension;
            File targetFile = new File(uploadFolder, newFileName);
            file.transferTo(targetFile);
            userChargeService.saveRealFileName(newFileName, charge, id, expendDate, categoryNo);

            return newFileName;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    @PostMapping("/{userchargeno}")
    public String modifyChargeData(
            @PathVariable long userchargeno,
            @RequestBody UserChargeAddDto userChargeAddDto
    ) {
        userChargeService.modifyChargeData(userchargeno, userChargeAddDto);
        return userChargeAddDto.getFileName();
    }

//    @GetMapping("/checkduplicate")
//    public ResponseEntity<?> checkDuplicate(
//            @RequestParam String id,
//            @RequestParam String expendDate,
//            @RequestParam(defaultValue="") Long userChargeNo) {
//        // id와 expendDate에 해당하는 레코드의 개수를 세어서 중복 여부를 판단
//        long duplicateCount = userChargeService.checkDuplicate(id, expendDate, userChargeNo);
//        boolean isDuplicate = duplicateCount > 0;
//
//        Map<String, Object> response = new HashMap<>();
//        response.put("duplicate", isDuplicate);
//
//        return ResponseEntity.ok(response);
//    }
}



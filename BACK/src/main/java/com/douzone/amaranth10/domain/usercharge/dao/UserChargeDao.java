package com.douzone.amaranth10.domain.usercharge.dao;

import com.douzone.amaranth10.domain.usercharge.dao.mapper.UserChargeMapper;
import com.douzone.amaranth10.domain.usercharge.dto.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;
import java.util.Map;

@Repository
@Slf4j
@RequiredArgsConstructor
public class UserChargeDao {
    private final UserChargeMapper userChargeMapper;

    public List<UserScheduleViewDto> userChargeSchedule() {
        return userChargeMapper.userChargeSchedule();
    }

    public List<UserChargeDto> userChargeList(String id, String searchValue, String searchField) {
        return userChargeMapper.userChargeList(id, searchValue, searchField);
    }

    public UserChargeDto userChargeDetail(long no) {
        return userChargeMapper.userChargeDetail(no);
    }

    public List<Map<String, List<String>>> userChargeCategoryDetail(long userChargeNo) {
        return userChargeMapper.userChargeCategoryDetail(userChargeNo);
    }

    public List<Map<String, List<String>>> categoryDetailForm(int categoryNo) {
        return userChargeMapper.categoryDetailForm(categoryNo);

    }

    public List<UserCategoryDto> userCategories() {
        return userChargeMapper.userCategories();
    }

    public String userChargeUpdate(List<Long> userChargeNo) {
        int size = userChargeNo.size();

        for (int i = 0; i < size; i++) {
            long userChargeNum = userChargeNo.get(i);
            if(userChargeMapper.findCharge(userChargeNum) != null){
            // userChargeMapper.userChargeUpdate 호출
            userChargeMapper.userChargeUpdate(userChargeNum);}
            else {
                return "deleted data"; // 결과가 null일 때 "deleted data" 반환
            }
        }
        return "success"; // 모든 작업이 성공했을 때 "success" 반환
    }

    @Transactional
    public void userChargeDelete(List<Long> userChargeNo) {
        int size = userChargeNo.size();

        for (int i = 0; i < size; i++) {
            long userChargeNum = userChargeNo.get(i);
            userChargeMapper.userChargeDelete(userChargeNum);
            userChargeMapper.userChargeDetailDelete(userChargeNum);
        }
    }

    @Transactional
    public void saveChargeData(UserChargeAddDto userChargeAddDto) {
        userChargeMapper.saveChargeData(userChargeAddDto);
        String id = userChargeAddDto.getId();
        String expendDate = userChargeAddDto.getExpendDate();
        long charge = userChargeAddDto.getCharge();
        int categoryNo = userChargeAddDto.getCategoryNo();

        Long userChargeNo = userChargeMapper.findChargeNo(id, expendDate, charge, categoryNo);
        saveChargeDetailData(userChargeAddDto.getItemContents(), userChargeNo);
    }

    private void saveChargeDetailData(List<UserChargeDetailAddDto> detailAddDtos, Long userChargeNo) {

        for (UserChargeDetailAddDto detailAddDto : detailAddDtos) {
            if (detailAddDto.getItemNo() != 100) {
                detailAddDto.setUserChargeNo(userChargeNo);
                try {
                    userChargeMapper.saveChargeDetail(detailAddDto);
                } catch (Exception e) {
                    throw new RuntimeException("상세 데이터 저장 중 오류 발생: " + e.getMessage());
                }
            }
        }
    }


//    public long checkDuplicate(String id, String expendDate, long userChargeNo) {
//        return userChargeMapper.checkDuplicate(id, expendDate, userChargeNo);
//    }

@Transactional
public void modifyChargeData(long userChargeNo, UserChargeAddDto userChargeAddDto) {
    try {
        modifyCharge(userChargeNo, userChargeAddDto);
    } catch (Exception e) {
        throw new RuntimeException("청구 내역 수정 중 오류 발생: " + e.getMessage());
    }
}

    private void modifyCharge(long userChargeNo, UserChargeAddDto userChargeAddDto) {
        String id = userChargeAddDto.getId();
        // 청구 내역 수정
        userChargeMapper.modifyChargeData(userChargeNo, userChargeAddDto);

        // 청구 상세 내역 초기화
        userChargeMapper.userChargeDetailDelete(userChargeNo);

        // user_charge_detail 테이블에 새로운 상세 내역 데이터 저장
        for (UserChargeDetailAddDto detailAddDto : userChargeAddDto.getItemContents()) {
            if (detailAddDto.getItemNo() != 100) { // itemNo가 100이 아닌 경우에만 처리
                detailAddDto.setUserChargeNo(userChargeNo);
                try {
                    userChargeMapper.saveChargeDetail(detailAddDto);
                } catch (Exception e) {
                    throw new RuntimeException("상세 데이터 저장 중 오류 발생: " + e.getMessage());
                }
            }
        }
    }

    public void updateRealFileName(String newFileName, long userChargeNo) {
        userChargeMapper.updateRealFileName(newFileName, userChargeNo);
    }

    public void saveRealFileName(String newFileName, long charge, String id, String expendDate, int categoryNo) {
        Long userChargeNo = userChargeMapper.findChargeNo(id, expendDate, charge, categoryNo);
        userChargeMapper.updateRealFileName(newFileName, userChargeNo);
    }

    public String ongoingSchedule() {
        return userChargeMapper.ongoingSchedule();
    }
}

package com.douzone.amaranth10.domain.creategroup.dao;

import com.douzone.amaranth10.domain.creategroup.dao.mapper.GroupMapper;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqGroupAndManagerDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqSearchGroupOptionDto;
import com.douzone.amaranth10.domain.creategroup.dto.response.ResDeptUserDto;
import com.douzone.amaranth10.domain.creategroup.dto.response.ResGroupDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqSearchGroupUserOptionDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqGroupAndMemberDto;
import com.douzone.amaranth10.domain.entity.Group;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
@RequiredArgsConstructor
public class GroupDao {
    private final GroupMapper groupMapper;

    public List<ResGroupDto> listGroup(ReqSearchGroupOptionDto dto) {
        return groupMapper.listGroup(dto);
    }

    public List<ResDeptUserDto> listGroupMember(ReqSearchGroupUserOptionDto dto) {
        return groupMapper.listGroupMember(dto);
    }

    public List<ResDeptUserDto> listGroupManager(ReqSearchGroupUserOptionDto dto) {
        return groupMapper.listGroupManager(dto);
    }

    public List<ResDeptUserDto> listDeptUser() {
        return groupMapper.listDeptUser();
    }

    public void insertGroup(ReqGroupAndMemberDto dto) {
        groupMapper.insertGroup(dto);
    }
    public void insertGroupMember(ReqGroupAndMemberDto dto) {
        groupMapper.insertGroupMember(dto);
    }

    public void deleteGroupMember(ReqGroupAndMemberDto dto) {
        groupMapper.deleteGroupMember(dto);
    }

    public void updateGroup(Group dto) {
        groupMapper.updateGroup(dto);
    }

    public void deleteGroup(int groupNo) {
        groupMapper.deleteGroup(groupNo);
    }

    public int checkBeforeUpdateGroupManager(List<String> insertList) {
        return groupMapper.checkBeforeUpdateGroupManager(insertList);
    }
    public void insertGroupManager(ReqGroupAndManagerDto dto) {
        groupMapper.insertGroupManager(dto);
    }

    public void deleteGroupManager(ReqGroupAndManagerDto dto) {
        groupMapper.deleteGroupManager(dto);
    }


    //퇴사했을때 그룹담당자테이블에서 삭제
    public void deleteResignGroupManager(String managerId) {
        System.out.println("groupDao의 삭제하려는 id:"+managerId);
        groupMapper.deleteResignGroupManager(managerId);
    }

}

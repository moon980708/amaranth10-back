package com.douzone.amaranth10.domain.createcategoryanditem.dao.mapper;

import com.douzone.amaranth10.domain.createcategoryanditem.dto.CategoryDto;
import com.douzone.amaranth10.domain.createcategoryanditem.dto.ItemDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface CategoryMapper {
    List<CategoryDto> categoryList();   // 용도 리스트 전체 출력
    List<CategoryDto> findCategory(String categoryName);   // 용도 리스트 검색 결과 출력
    void insertCategory(CategoryDto categoryDto); // 용도 추가
    void matchingItemPrice(HashMap<String, Object> updateMap);   // 용도 추가 시 100번 항목 추가
    void updateCategory(HashMap<String, Object> updateMap);   // 용도 사용여부 수정
    void deleteCategory(int categoryNo);   // 용도 삭제
    List<Integer> notDeleteCategory(int categoryNo);   // 용도 삭제 불가능 리스트 (= 청구내역이 있는 용도)
    void deleteItemPrice(int categoryNo);   // 용도 삭제 시 100번 항목 삭제
    List<ItemDto> itemList();   // 항목 리스트 전체 출력
    List<ItemDto> findItem(String itemTitle);   // 항목 리스트 검색 결과 출력
    void insertItem(ItemDto itemDto); // 항목 추가
    //    void updateItemDelete(HashMap<String, Object> updateMap);   // 항목 삭제 (soft delete)
    void deleteItem(int itemNo);   // 항목 삭제 (hard Delete)
    List<Integer> notDeleteItem(int itemNo);   // 항목 삭제 불가능 리스트 (= 청구내역이 있는 항목)
    void deleteItemOption(int itemNo);   // 항목 삭제 시 매칭 테이블에서도 삭제






//    UserVO fetchUserByID(int id);
//    void deleteUser(int id);   //삭제
}
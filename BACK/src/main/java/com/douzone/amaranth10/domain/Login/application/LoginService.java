package com.douzone.amaranth10.domain.Login.application;

import com.douzone.amaranth10.domain.Login.dao.LoginDao;
import com.douzone.amaranth10.domain.Login.dto.LoginInfoDto;
import com.douzone.amaranth10.global.util.EncryptorsUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final BCryptPasswordEncoder passwordEncoder;

    private final LoginDao loginDao;

    public void encryptId(String id){
        System.out.println("암호화된 것 : " + passwordEncoder.encode(id));
        System.out.println("매칭된건지 : " +passwordEncoder.matches(id,passwordEncoder.encode(id)));
    }

    public void emailTest(String email){
        String encryptedEmail = EncryptorsUtils.encrypt(email);

        System.out.println("암호화 Email: " + encryptedEmail);
        System.out.println("복호화 Email: " + EncryptorsUtils.decrypt(encryptedEmail));
    }
    public String realLogin(String id, String password){
        System.out.println("id = " + id);
        System.out.println("password = " + password);
        String getPassword = loginDao.getPassword(id);//아이디에 해당하는 디비에 저장된 암호화된 비밀번호


        System.out.println("getPassword = " + getPassword);
        if(passwordEncoder.matches(password,getPassword)){
            System.out.println("로그인 성공 : " + id);
            return "loginSuccess";
        }
        else{
            return "loginFail";
        }

    }
    public LoginInfoDto getLoginInfo(String id){

        return loginDao.getLoginInfo(id);
    }

    public String checkGroupManager(String id) {
        return loginDao.checkGroupManager(id);
    }
}

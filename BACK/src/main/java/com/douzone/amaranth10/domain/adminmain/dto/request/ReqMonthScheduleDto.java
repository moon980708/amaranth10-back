package com.douzone.amaranth10.domain.adminmain.dto.request;

import lombok.Data;

@Data
public class ReqMonthScheduleDto {
    private String chargeMonth;
}

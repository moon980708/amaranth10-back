package com.douzone.amaranth10.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@NoArgsConstructor
@Getter
@Setter
public class UserCharge {
    private long userChargeNo;

    private int categoryNo;
    private String id;
    private int scheduleNo;
    private Timestamp expendDate;
    private String store;
    private long charge;
    private String contents;
    private String payment;
    private Timestamp userChargeDate;
    private String state;
    private boolean rejectCheck;
    private String fileName;
    private String rejectReason;

}

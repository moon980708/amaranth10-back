package com.douzone.amaranth10.domain.schedule.dao;


import com.douzone.amaranth10.domain.schedule.dto.*;
import com.douzone.amaranth10.domain.schedule.dao.mapper.ScheduleMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Slf4j
@RequiredArgsConstructor
public class ScheduleDao {

    private final ScheduleMapper scheduleMapper;

    public Integer scheduleLoad(String chargeDateString){
        return scheduleMapper.scheduleLoad(chargeDateString);
    }
    public HashMap<String, Object> scheduleChargeEnd(HashMap<String,Object> map){
        return scheduleMapper.scheduleChargeEnd(map);
    }
    public List<ScheduleListDto> yearMonthList(String chargeDateString){
        return scheduleMapper.yearMonthList(chargeDateString);
    }

    public void updateScheduleClose(HashMap<String, Object> map){
        scheduleMapper.updateScheduleClose(map);
    }

    public void createNewSchedule(ScheduleInsertDto dto){
        scheduleMapper.createNewSchedule(dto);
    }

    public void scheduleDelete(int scheduleNo){
        scheduleMapper.scheduleDelete(scheduleNo);
    }
    public List<ScheduleAboutInfoDto> getScheduleModifyAboutInfo(HashMap<String,Object> map){
        return scheduleMapper.getScheduleModifyAboutInfo(map);
    }
    public void updateCloseToCloseScheduler(Integer scheduleNo){
        scheduleMapper.updateCloseToCloseScheduler(scheduleNo);
    }

    public void updateCloseToBeginScheduler(Integer scheduleNo){
        scheduleMapper.updateCloseToBeginScheduler(scheduleNo);
    }
    public void modifySchedule(ScheduleModifyDto dto){
        scheduleMapper.modifySchedule(dto);
    }
    public List<OngoingScheduleInfoDto> isOngoingSchedule(){
        return scheduleMapper.isOngoingSchedule();
    }
    public void deleteRegisterCharge(Integer scheduleNo){
        scheduleMapper.deleteRegisterCharge(scheduleNo);
    }
}

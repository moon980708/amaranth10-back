package com.douzone.amaranth10.domain.createcategoryanditem.dao;

import com.douzone.amaranth10.domain.createcategoryanditem.dto.CategoryDto;
import com.douzone.amaranth10.domain.createcategoryanditem.dto.ItemDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.douzone.amaranth10.domain.createcategoryanditem.dao.mapper.CategoryMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Repository
@Slf4j
@RequiredArgsConstructor
public class CategoryDao {
    private final CategoryMapper categoryMapper;

    public List<CategoryDto> categorylist() {
        return categoryMapper.categoryList();
    }

    // 용도 검색 기능 추가
    public List<CategoryDto> findCategory(String categoryName) {
        return categoryMapper.findCategory(categoryName);
    }

    public void insertCategory(CategoryDto categoryDto) {
        categoryMapper.insertCategory(categoryDto);
    }

    public void matchingItemPrice(HashMap<String, Object> updateMap) { categoryMapper.matchingItemPrice(updateMap); }

    public void updateCategory(HashMap<String, Object> updateMap) {
        categoryMapper.updateCategory(updateMap);
    }

    public void deleteCategory(int categoryNo) { categoryMapper.deleteCategory(categoryNo); }

    public List<Integer> notDeleteCategory(int categoryNo) {
//        List<Integer> notDeleteList = new ArrayList<>();
//
//        if(categoryMapper.notDeleteCategory(categoryNo) > 0) {   // 청구내역이 있는 경우
//            return categoryNo;
//        }
        return categoryMapper.notDeleteCategory(categoryNo);
    }

    public void deleteItemPrice(int categoryNo) {
        categoryMapper.deleteItemPrice(categoryNo);
    }

    public List<ItemDto> itemlist() {
        return categoryMapper.itemList();
    }

    public List<ItemDto> findItem(String itemTitle) {
        return categoryMapper.findItem(itemTitle);
    }

    public void insertItem(ItemDto itemDto) {
        categoryMapper.insertItem(itemDto);
    }

//    public void updateItemDelete(HashMap<String, Object> updateMap) { categoryMapper.updateItemDelete(updateMap); }

    public void deleteItem(int itemNo) { categoryMapper.deleteItem(itemNo); }

    public List<Integer> notDeleteItem(int itemNo) {
        return categoryMapper.notDeleteItem(itemNo);
    }

    public void deleteItemOption(int itemNo) {
        categoryMapper.deleteItemOption(itemNo);
    }





//    세부
//    public List<CategoryDto> categorylistByCategoryNo(int categoryNo) {
//        return CategoryMapper.categoryListByCategoryNo(categoryNo);
//    }


}

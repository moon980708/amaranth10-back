package com.douzone.amaranth10.domain.usercharge.dao.mapper;

import com.douzone.amaranth10.domain.usercharge.dto.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserChargeMapper {

    List<UserScheduleViewDto> userChargeSchedule();

    List<UserChargeDto> userChargeList(
            @Param("id") String id,
            @Param("searchValue") String searchValue,
            @Param("searchField") String searchField
    );

    UserChargeDto userChargeDetail(@Param("no") long no);

    List<Map<String, List<String>>> userChargeCategoryDetail(
            @Param("userChargeNo") long userChargeNo
    );

    List<Map<String, List<String>>> categoryDetailForm(
            @Param("categoryNo") int categoryNo
    );

    List<UserCategoryDto> userCategories();

    void userChargeUpdate(@Param("userChargeNum") long userChargeNum);

    void userChargeDelete(@Param("userChargeNum") long userChargeNum);

    void saveChargeData(UserChargeAddDto userChargeAddDto);

    Long findCharge(@Param("userChargeNum") long userChargeNum);

    Long findChargeNo(
            @Param("id") String id,
            @Param("expendDate") String expendDate,
            @Param("charge") long charge,
            @Param("categoryNo") int categoryNo);

    void saveChargeDetail(UserChargeDetailAddDto detailAddDto);

    void userChargeDetailDelete(long userChargeNum);

//    long checkDuplicate(String id, String expendDate, long userChargeNo);

    void modifyChargeData(
            @Param("userchargeno") long userchargeno,
            @Param("userChargeAddDto") UserChargeAddDto userChargeAddDto);

    void updateRealFileName(@Param("newFileName") String newFileName, @Param("userChargeNo") long userChargeNo);
    String ongoingSchedule();
}

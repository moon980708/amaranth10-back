package com.douzone.amaranth10.domain.matching.controller;

import com.douzone.amaranth10.domain.matching.application.MatchingService;
import com.douzone.amaranth10.domain.matching.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class MatchingController {

    private final MatchingService matchingService;

    @GetMapping("/getCategoryList")
    public List<CategoryListDto> getCategoryList(){
        List<CategoryListDto> categoryList = matchingService.getCategoryList();
        System.out.println("categoryList = " + categoryList);
        return matchingService.getCategoryList();
    }

    @GetMapping("/getItemList")
    public List<ItemListDto> getItemList(){
        return matchingService.getItemList();
    }
    @PostMapping("/categoryMatching")
    public void checkMatchingItemList(@RequestBody @Valid MatchingInfoDto dto){

//        public void matchingItemList(@RequestBody MatchingInfoDto dto) {
        List<Integer> itemNoList = dto.getItemNoList();
        List<Integer> itemOptionList = dto.getItemOptionList();
        List<Boolean> itemEssentialList = dto.getItemEssentialList();
        Integer categoryNo = dto.getCategoryNo();
        System.out.println("itemNoList = " + itemNoList);
        System.out.println("itemOptionList = " + itemOptionList);
        System.out.println("itemEssentialList = " + itemEssentialList);
        List<HashMap<String, Object>> dataList = new ArrayList<>();

        for (int i = 0; i < itemNoList.size(); i++) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("categoryNo", categoryNo);
            map.put("itemNoList", itemNoList.get(i));
            map.put("itemOptionList", itemOptionList.get(i));
            map.put("itemEssentialList", itemEssentialList.get(i));
            dataList.add(map);
        }
        matchingService.checkMatchingItemList(categoryNo, dataList);
    }
    @GetMapping("/getSelectCategoryAboutItemList")
    public List<CategoryAboutItemListDto> getSelectCategoryAboutItemList(Integer categoryNo) {
        System.out.println("MatchingController.getSelectCategoryAboutItemList");
        return matchingService.getSelectCategoryAboutItemList(categoryNo);
    }

    @GetMapping("/searchCategory")
    public List<CategoryListDto> searchCategory(String keyword){
        System.out.println("MatchingController.searchCategory");
        return matchingService.searchCategory(keyword);
    }

    @GetMapping("/searchItem")
    public List<ItemListDto> searchItem(String keyword){
        System.out.println("MatchingController.searchItem");
        return matchingService.searchItem(keyword);
    }


}

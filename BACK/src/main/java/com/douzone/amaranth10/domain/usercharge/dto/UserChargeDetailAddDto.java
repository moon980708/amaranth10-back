package com.douzone.amaranth10.domain.usercharge.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@RequiredArgsConstructor
@ToString
public class UserChargeDetailAddDto {
    private long userChargeNo;
    private int itemNo;
    private String itemTitle;
    private String itemContentsValue;
}

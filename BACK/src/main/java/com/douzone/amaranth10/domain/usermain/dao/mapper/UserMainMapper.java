package com.douzone.amaranth10.domain.usermain.dao.mapper;

import com.douzone.amaranth10.domain.usermain.dto.*;
import com.douzone.amaranth10.domain.usermain.dto.UserInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMainMapper {

    UserInfoDto userInfo(@Param("id") String id);

    void userInfoUpdate(UserInfoModifyDto userInfoModifyDto);

    List<ChargeSummaryDto> userChargeSummary(@Param("id") String id);

    List<CategoryChartDto> userCategoryChart(@Param("id") String id, @Param("year") int year, @Param("month") int month);

    List<MonthlyChartDto> userMonthlyChart(@Param("id") String id);

    public String getPassword(String id);
    void userInfoEmailUpdate(UserUpdateEmailDto emailDto);
    void userInfoPasswordUpdate(UserUpdatePasswordDto passwordDto);

}

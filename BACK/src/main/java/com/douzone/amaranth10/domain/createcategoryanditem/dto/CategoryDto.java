package com.douzone.amaranth10.domain.createcategoryanditem.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class CategoryDto {
    private int categoryNo;
    private String categoryName;
    private boolean usable;
}

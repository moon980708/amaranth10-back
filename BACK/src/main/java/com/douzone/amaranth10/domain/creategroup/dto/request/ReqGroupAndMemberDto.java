package com.douzone.amaranth10.domain.creategroup.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReqGroupAndMemberDto {
    private int groupNo;
    private String groupName;

    private List<String> insertList;
    private List<String> deleteList;
}

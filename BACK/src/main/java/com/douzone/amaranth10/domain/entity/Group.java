package com.douzone.amaranth10.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Group {
    private int groupNo;
    private String groupName;
    private boolean usable;
}

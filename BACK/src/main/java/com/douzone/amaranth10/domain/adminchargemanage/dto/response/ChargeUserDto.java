package com.douzone.amaranth10.domain.adminchargemanage.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChargeUserDto {
    private String id;
    private String userName;
    private String deptName;
    private boolean resign;

    private int count;
    private long sum;

    private Timestamp searchTime;     //조회시간
}

package com.douzone.amaranth10.domain.adminusermanage.controller;

import com.douzone.amaranth10.domain.adminusermanage.application.UserManageService;
import com.douzone.amaranth10.domain.adminusermanage.dao.UserManageDao;
import com.douzone.amaranth10.domain.adminusermanage.dao.mapper.UserManageMapper;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserAddDto;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserFindDto;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserManageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UserManageController {
    @Autowired
    private UserManageService userManageService;

    // 프론트 페이징
//    @GetMapping("/admin/user")
//    public List<UserManageDto> userList(@RequestParam(required = false) Integer deptNo,
//                                        @RequestParam(required = false) Integer rankNo,
//                                        @RequestParam(required = false) Boolean resign,
//                                        @RequestParam(required = false) String keyword) {
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("deptNo", deptNo);
//        map.put("rankNo", rankNo);
//        map.put("resign", resign);
//        map.put("keyword", keyword);
//
//        List<UserManageDto> userList;
//
//        if (deptNo != null || rankNo != null || resign != null || (keyword != null && !keyword.isEmpty())) {
//            System.out.println("성공 map = " + map);
//            userList = userManageService.findUser(map);
//        } else {
//            System.out.println("전체검색 map = " + map);
//            userList = userManageService.userList();
//        }
//        return userList;
//    }

    // 백엔드 페이징
    @GetMapping("/admin/user")
    public List<UserManageDto> userList(@RequestParam Integer page,
                                        @RequestParam(required = false) Integer deptNo,
                                        @RequestParam(required = false) Integer rankNo,
                                        @RequestParam(required = false) Boolean resign,
                                        @RequestParam(required = false) String keyword) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("deptNo", deptNo);
        map.put("rankNo", rankNo);
        map.put("resign", resign);
        map.put("keyword", keyword);

        List<UserManageDto> userList;

//        if (deptNo != null || rankNo != null || resign != null || (keyword != null && !keyword.isEmpty())) {
            System.out.println("검색성공 map = " + map);
            userList = userManageService.findUser(page, map);
//        }
//        else {
//            System.out.println("전체검색 map = " + map);
//            System.out.println("page : " + page);
//            userList = userManageService.userList(page);
//
//            System.out.println("userList = " + userList);
//        }
        return userList;
    }
    @GetMapping("/admin/user/save")
    public List<UserManageDto> userList(
                                        @RequestParam(required = false) Integer deptNo,
                                        @RequestParam(required = false) Integer rankNo,
                                        @RequestParam(required = false) Boolean resign,
                                        @RequestParam(required = false) String keyword) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("deptNo", deptNo);
        map.put("rankNo", rankNo);
        map.put("resign", resign);
        map.put("keyword", keyword);

        List<UserManageDto> userList;

        System.out.println("저장 검색성공 map = " + map);
        userList = userManageService.saveUser(map);

        return userList;
    }
    @GetMapping("/admin/user/getsearchcount")
    public Integer getSearchCount(@RequestParam(required = false) Integer deptNo,
                                  @RequestParam(required = false) Integer rankNo,
                                  @RequestParam(required = false) Boolean resign,
                                  @RequestParam(required = false) String keyword) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("deptNo", deptNo);
        map.put("rankNo", rankNo);
        map.put("resign", resign);
        map.put("keyword", keyword);

        List<UserManageDto> userList;

//        if (deptNo != null || rankNo != null || resign != null || (keyword != null && !keyword.isEmpty())) {
            System.out.println("검색 map = " + map);
//        } else {
//            System.out.println("전체검색 map = " + map);
//            userList = userManageService.userList();
//
//            System.out.println("userList = " + userList);
//        }
        return userManageService.getSearchCount(map);
    }


    @PostMapping("/admin/user/add")
    public void insertUser(@RequestBody UserAddDto userAddDto) {
        userManageService.insertUser(userAddDto);
    }

    @GetMapping("/admin/user/dept")
    public List<String> userDept() {
        return userManageService.userDept();
    }

    @GetMapping("/admin/user/rank")
    public List<String> userRank() {
        return userManageService.userRank();
    }

    @PutMapping("/admin/user/update/{id}")   // 사원 수정
    public void updateUser(@RequestBody HashMap<String, Object> map, @PathVariable String id) {
        String userName = (String) map.get("userName");
        Integer deptNo = (Integer) map.get("deptNo");
        Integer rankNo = (Integer) map.get("rankNo");
        String email = (String) map.get("email");
//        Boolean resign = (boolean) map.get("resign");

        HashMap<String, Object> updateMap = new HashMap<>();
        updateMap.put("id", id);
        updateMap.put("userName", userName);
        updateMap.put("deptNo", deptNo);
        updateMap.put("rankNo", rankNo);
        updateMap.put("email", email);
//        updateMap.put("resign", resign);

        userManageService.updateUser(updateMap);
    }

    @PutMapping("/admin/user/update/resign/{id}")   // 사원 퇴사여부 수정
    public void updateResign(@RequestBody HashMap<String, Object> map, @PathVariable String id) {
        boolean resign = (boolean) map.get("resign");

        HashMap<String, Object> updateMap = new HashMap<>();
        updateMap.put("id", id);
        updateMap.put("resign", !resign);

        System.out.println("updateMap = " + updateMap);

        userManageService.updateResign(updateMap);
    }

    @PutMapping("/admin/user/update/resignAll")
    public void updateAllResign(@RequestBody ArrayList<String> checkedId) {
        for(String id : checkedId){
            System.out.println("id = " + id);

            HashMap<String, Object> updateMap = new HashMap<>();
            updateMap.put("id", id);
            updateMap.put("resign", true);

            System.out.println("updateMap = " + updateMap);

            userManageService.updateResign(updateMap);
        }
    }















    // 다른 API를 통해 categoryNo를 받고 필요한 경우에 사용
//    @GetMapping("/admin/categorylistByCategoryNo")
//    public List<CategoryDto> categorylistByCategoryNo(@RequestParam int categoryNo) {
//        return categoryService.categorylistByCategoryNo(categoryNo);
//    }

//    검색
//    @GetMapping("/api/boardList")
//    public List<Object> findBoard(
//            @RequestParam(defaultValue = "1") int page,
//            @RequestParam(defaultValue = "10") int pageSize,
//            @RequestParam(defaultValue = "") String category,
//            @RequestParam(defaultValue = "") String keyword
//    ) {
//        List<Object> list = new ArrayList<>();
//        List<BoardVo> boardList = boardService.findBoard(page, pageSize, category, keyword);
//        list.add(boardList);
//        int countList = boardService.countBoard(category, keyword);
//        list.add(countList);
//        return list;
//    }
}
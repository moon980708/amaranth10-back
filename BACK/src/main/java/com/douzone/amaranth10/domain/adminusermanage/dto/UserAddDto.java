package com.douzone.amaranth10.domain.adminusermanage.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class UserAddDto {
    private String id;
    private String password;
    private String userName;
    private String deptNo;
    private String rankNo;
    private String email;
}

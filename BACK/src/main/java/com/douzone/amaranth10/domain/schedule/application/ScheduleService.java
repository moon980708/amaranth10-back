package com.douzone.amaranth10.domain.schedule.application;

import com.douzone.amaranth10.domain.schedule.dao.ScheduleDao;
import com.douzone.amaranth10.domain.schedule.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class ScheduleService {

    private final ScheduleDao scheduleDao;

    //동작확인 완료
    @Transactional
    @Scheduled(cron = "59 59 23 * * ? ")
    public void updateCloseToCloseScheduler() {
        LocalDate today = LocalDate.now();
        List<ScheduleListDto> schedules = yearMonthList(today.format(DateTimeFormatter.ofPattern("yyyy-MM"))); // 해당 월의 스케줄 목록 가져오기

        for (ScheduleListDto dto : schedules) {
            if (dto.getChargeEnd().toLocalDate().equals(LocalDateTime.now().toLocalDate())) {
                Integer scheduleNo = dto.getScheduleNo();
                System.out.println("마감일 같은 차수 NO : " + scheduleNo);
                scheduleDao.updateCloseToCloseScheduler(scheduleNo);

                scheduleDao.deleteRegisterCharge(scheduleNo);
//                break;
            }

            if (dto.getChargeStart().toLocalDate().equals(LocalDateTime.now().toLocalDate().plusDays(1))) {
                Integer scheduleNo = dto.getScheduleNo();
                System.out.println("시작일 같은 차수 NO : " + scheduleNo);
                scheduleDao.updateCloseToBeginScheduler(scheduleNo);
//                break;
            }
        }
    }

    public Integer scheduleLoad(String chargeDateString) {
        return scheduleDao.scheduleLoad(chargeDateString);
    }


    public HashMap<String, Object> scheduleChargeEnd(HashMap<String, Object> map) {
        return scheduleDao.scheduleChargeEnd(map);
    }


    public List<ScheduleListDto> yearMonthList(String chargeDateString) {
        return scheduleDao.yearMonthList(chargeDateString);
    }

    @Transactional
    public void updateScheduleClose(HashMap<String, Object> map){
        scheduleDao.updateScheduleClose(map);
    }

    @Transactional
    public void createNewSchedule(ScheduleInsertDto dto){
        scheduleDao.createNewSchedule(dto);

    }

    public void scheduleDelete(int scheduleNo){
        scheduleDao.scheduleDelete(scheduleNo);
    }

    @Transactional
    public void scheduleDeleteCheckAll(List<Integer> selectScheduleNo){
        for(int no : selectScheduleNo){
            System.out.println("deleteCheckAll no = " + no);
            scheduleDao.scheduleDelete(no);
        }
    }

    public List<ScheduleAboutInfoDto>getScheduleModifyAboutInfo(HashMap<String,Object> map){
        List<ScheduleAboutInfoDto> list = scheduleDao.getScheduleModifyAboutInfo(map);
        System.out.println(" service list = " + list);
        list.removeIf(Objects::isNull);

        if(list.isEmpty()){
            list.add(new ScheduleAboutInfoDto());
        }
        return list;
    }

    @Transactional
    public void modifySchedule(ScheduleModifyDto dto){
        System.out.println("ScheduleService.modifySchedule");
        scheduleDao.modifySchedule(dto);
    }

    public List<OngoingScheduleInfoDto> isOngoingSchedule(){
        List<OngoingScheduleInfoDto> result = scheduleDao.isOngoingSchedule();

        System.out.println("result = " + result);
        if (result.isEmpty()) {
            System.out.println("isOngoingSchedule is null");
            return null;
        }

        return result;
    }


}

package com.douzone.amaranth10.domain.adminmain.dao.mapper;

import com.douzone.amaranth10.domain.adminmain.dto.request.ReqMonthDeptDto;
import com.douzone.amaranth10.domain.adminmain.dto.request.ReqMonthScheduleDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.MonthDeptDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.ResWaitDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.BaseAnalysisDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.MonthlyChargeDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.UserChargeRankDto;
import com.douzone.amaranth10.domain.adminmain.dto.request.AnalysisOptionDto;
import com.douzone.amaranth10.domain.adminmain.dto.request.ReqAnalysisOptionDto;
import com.douzone.amaranth10.domain.entity.Dept;
import com.douzone.amaranth10.domain.entity.Group;
import com.douzone.amaranth10.domain.entity.Schedule;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdminMainMapper {
    public ResWaitDto listWait(String managerId);
    public MonthDeptDto totalMonthConfirm(ReqMonthDeptDto dto);
    public List<MonthDeptDto> listMonthDept5(ReqMonthDeptDto dto);
    public List<Schedule> listMonthSchedule(ReqMonthScheduleDto dto);

    //경비현황분석
    public List<Group> listUsableGroup(String managerId);
    public List<Dept> listDept();
    public BaseAnalysisDto totalGroupDeptByPeriod(ReqAnalysisOptionDto dto);
    public BaseAnalysisDto mostCountDeptByPeriod(ReqAnalysisOptionDto dto);
    public List<BaseAnalysisDto> listCategoryByPeriod(ReqAnalysisOptionDto dto);
    public List<MonthlyChargeDto> monthlyApplyCharge(ReqAnalysisOptionDto dto);
    public List<MonthlyChargeDto> monthlyConfirmCharge(ReqAnalysisOptionDto dto);
    public List<UserChargeRankDto> listUserChargeRankByPeriod(AnalysisOptionDto dto);


    // 권한체크
    public String getUserResignCheck(String id);
    public String getGroupManagerCheck(String id);

}

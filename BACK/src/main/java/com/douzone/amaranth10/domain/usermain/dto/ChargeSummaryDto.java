package com.douzone.amaranth10.domain.usermain.dto;

import lombok.*;

@Data
public class ChargeSummaryDto {
    private Long approvedSum;
    private Long pendingSum;
    private Long rejectedSum;
    private int cmp;
    private int ing;
    private int fail;
    private int year;
    private int month;
}
package com.douzone.amaranth10.domain.usercharge.dto;

import lombok.*;

@Data
public class UserChargeDto {
    private long userChargeNo;
    private String expendDate;
    private String categoryName;
    private int categoryNo;
    private String store;
    private long charge;
    private String contents;
    private String payment;
    private String state;
    private String fileName;
    private String realFileName;
    private String filePath;
}

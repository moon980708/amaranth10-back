package com.douzone.amaranth10.domain.adminmain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResAnalysisByGroupDeptByPeriodDto {
    private BaseAnalysisDto totalConfirm;
    private BaseAnalysisDto mostCountDept;
    
    private List<BaseAnalysisDto> categoryList;   //용도리스트 (총금액,건수)

    private List<MonthlyChargeDto> monthlyApplyCharge;
    private List<MonthlyChargeDto> monthlyConfirmCharge;

    private List<UserChargeRankDto> userRankBySum;    //청구자 금액순
    private List<UserChargeRankDto> userRankByCount;  //청구자 건수순



}

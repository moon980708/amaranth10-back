package com.douzone.amaranth10.domain.adminchargemanage.dto.response;

import lombok.Data;

import java.sql.Date;
import java.util.List;

@Data
public class ResChargeDto {
    //공통
    private long userChargeNo;
    private int categoryNo;
    private String id;
    private int scheduleNo;
    private String expendDate;
    private String store;
    private long charge;
    private String contents;
    private String payment;
    private Date userChargeDate;
    private String state;
    private boolean rejectCheck;
    private String fileName;
    private String realFileName;
    private String rejectReason;


    private String userName;
    private String deptName;
    private String categoryName;

    private int totalCount;   //총 내역 갯수

    //상세
    private String scheduleName;      //0000년 00월 0차
    private List<ItemDto> itemList;       //항목이름, 항목내용
    private String email;     //사용자 이메일

}

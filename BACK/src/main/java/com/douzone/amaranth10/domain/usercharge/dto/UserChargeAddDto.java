package com.douzone.amaranth10.domain.usercharge.dto;

import lombok.*;

import java.util.List;

@Getter @Setter
@RequiredArgsConstructor
@ToString
public class UserChargeAddDto {
    private int categoryNo;
    private String id;
    private int scheduleNo;
    private String expendDate;
    private String store;
    private long charge;
    private String contents;
    private String payment;
    private String fileName;
    private List<UserChargeDetailAddDto> itemContents;

}

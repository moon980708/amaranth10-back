package com.douzone.amaranth10.domain.test.dao;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.douzone.amaranth10.domain.test.dao.mapper.testMapper;
@Repository
@Slf4j
@RequiredArgsConstructor
public class testDao {

    @Autowired
    private testMapper testMapper;

    public String userName(){
        return testMapper.userName();
    }
}

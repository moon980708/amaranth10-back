package com.douzone.amaranth10.domain.adminmain.dto.response;

import lombok.Data;

import java.math.BigInteger;

@Data
public class UserChargeRankDto {
    private int rank;
    private String id;
    private String userName;
    private String deptName;
    private int count;
    private BigInteger sum;
    private boolean resign;
}

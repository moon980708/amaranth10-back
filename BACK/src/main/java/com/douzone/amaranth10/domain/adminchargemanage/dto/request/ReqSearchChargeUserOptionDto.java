package com.douzone.amaranth10.domain.adminchargemanage.dto.request;

import lombok.Data;

@Data
public class ReqSearchChargeUserOptionDto {
    private String managerId;
    private int scheduleNo;
    private int groupDeptNo;
    private String state;
}

package com.douzone.amaranth10.domain.adminusermanage.dao;

import com.douzone.amaranth10.domain.adminusermanage.dao.mapper.UserManageMapper;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserAddDto;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserFindDto;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserManageDto;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserSaveDto;
import com.douzone.amaranth10.domain.usercharge.dto.UserChargeDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Slf4j
@RequiredArgsConstructor
public class UserManageDao {
    private final UserManageMapper userManageMapper;

    //    public List<UserManageDto> userList() {
//        return userManageMapper.userList();
//    }
    public List<UserManageDto> userList(Integer offset) {
        return userManageMapper.userList(offset);
    }

    public Integer getSearchCount(HashMap<String, Object> map) { return userManageMapper.getSearchCount(map); }
    public List<String> userDept() {
        return userManageMapper.userDept();
    }
    public List<String> userRank() {
        return userManageMapper.userRank();
    }
    public List<UserManageDto> findUser(UserFindDto dto) {
        return userManageMapper.findUser(dto);
    }

    public List<UserManageDto> saveUser(UserSaveDto dto) {
        return userManageMapper.saveUser(dto);
    }
    public void insertUser(UserAddDto userAddDto) {
        userManageMapper.insertUser(userAddDto);
    }
    public void updateUser(HashMap<String, Object> updateMap) {
        userManageMapper.updateUser(updateMap);
    }
    public void updateResign(HashMap<String, Object> updateMap) {
        userManageMapper.updateResign(updateMap);
    }
//    public void updateAllResign(HashMap<String, Object> updateMap) { userManageMapper.updateAllResign(updateMap); }
}

package com.douzone.amaranth10.domain.test.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class testServiceImpl implements testService{

    @Autowired
    private com.douzone.amaranth10.domain.test.dao.testDao testDao;
    @Override
    public String userName() {
        return testDao.userName();
    }
}

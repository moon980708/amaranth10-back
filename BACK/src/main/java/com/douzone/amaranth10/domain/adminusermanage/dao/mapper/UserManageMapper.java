package com.douzone.amaranth10.domain.adminusermanage.dao.mapper;

import com.douzone.amaranth10.domain.adminusermanage.dto.UserAddDto;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserFindDto;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserManageDto;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserSaveDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface UserManageMapper {
    List<UserManageDto> userList(Integer offset);   // 사원 리스트 전체 출력
    Integer getSearchCount(HashMap<String, Object> map);    // 페이징 위한 검색 사원 count (전체 or 검색)
    List<String> userDept();   // 부서 목록 출력
    List<String> userRank();   // 직급 목록 출력
    List<UserManageDto> findUser(UserFindDto dto);   // 사원 검색 결과 출력
    List<UserManageDto> saveUser(UserSaveDto dto);   // 사원 검색 결과 출력

    void insertUser(UserAddDto userAddDto);   // 사원 추가
    void updateUser(HashMap<String, Object> updateMap);   // 사원 정보 수정
    void updateResign(HashMap<String, Object> updateMap);   // 사원 퇴사여부 수정 (개별, 단체)
}
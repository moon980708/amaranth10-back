package com.douzone.amaranth10.domain.matching.dao.mapper;

import com.douzone.amaranth10.domain.matching.dto.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface MatchingMapper {
    public List<CategoryListDto> getCategoryList();
    public List<ItemListDto> getItemList();
    public Integer checkMatchingItemList(HashMap<String, Object> map);
    public void insertMatchingItemList(HashMap<String,Object> map);
    public void updateMatchingItemList(HashMap<String,Object> map);
    public void deleteMatchingItemList(HashMap<String,Object> map);
    public List<CategoryAboutItemListDto> getSelectCategoryAboutItemList(Integer categoryNo);
    public List<CategoryListDto> searchCategory(String keyword);
    public List<ItemListDto> searchItem(String keyword);
}

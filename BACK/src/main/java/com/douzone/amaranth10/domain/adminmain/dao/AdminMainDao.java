package com.douzone.amaranth10.domain.adminmain.dao;

import com.douzone.amaranth10.domain.adminmain.dto.request.ReqMonthDeptDto;
import com.douzone.amaranth10.domain.adminmain.dto.request.ReqMonthScheduleDto;
import com.douzone.amaranth10.domain.adminmain.dao.mapper.AdminMainMapper;
import com.douzone.amaranth10.domain.adminmain.dto.response.MonthDeptDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.ResWaitDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.BaseAnalysisDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.MonthlyChargeDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.UserChargeRankDto;
import com.douzone.amaranth10.domain.adminmain.dto.request.AnalysisOptionDto;
import com.douzone.amaranth10.domain.adminmain.dto.request.ReqAnalysisOptionDto;
import com.douzone.amaranth10.domain.entity.Dept;
import com.douzone.amaranth10.domain.entity.Group;
import com.douzone.amaranth10.domain.entity.Schedule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
@RequiredArgsConstructor
public class AdminMainDao {

    private final AdminMainMapper adminMainMapper;

    public ResWaitDto listWait(String managerId) {
        return adminMainMapper.listWait(managerId);
    }
    public MonthDeptDto totalMonthConfirm(ReqMonthDeptDto dto) {
        return adminMainMapper.totalMonthConfirm(dto);
    }
    public List<MonthDeptDto> listMonthDept5(ReqMonthDeptDto dto) {
        return adminMainMapper.listMonthDept5(dto);
    }
    public List<Schedule> listMonthSchedule(ReqMonthScheduleDto dto) {
        return adminMainMapper.listMonthSchedule(dto);
    }

    public List<Group> listUsableGroup(String managerId) {
        return adminMainMapper.listUsableGroup(managerId);
    }
    public List<Dept> listDept() {
        return adminMainMapper.listDept();
    }

    public BaseAnalysisDto totalGroupDeptByPeriod(ReqAnalysisOptionDto dto) {
        return adminMainMapper.totalGroupDeptByPeriod(dto);
    }
    public BaseAnalysisDto mostCountDeptByPeriod(ReqAnalysisOptionDto dto) {
        return adminMainMapper.mostCountDeptByPeriod(dto);
    }
    public List<BaseAnalysisDto> listCategoryByPeriod(ReqAnalysisOptionDto dto) {
        return adminMainMapper.listCategoryByPeriod(dto);
    }
    public List<MonthlyChargeDto> monthlyApplyCharge(ReqAnalysisOptionDto dto) {
        return adminMainMapper.monthlyApplyCharge(dto);
    }
    public List<MonthlyChargeDto> monthlyConfirmCharge(ReqAnalysisOptionDto dto) {
        return adminMainMapper.monthlyConfirmCharge(dto);
    }

    public List<UserChargeRankDto> listUserChargeRankByPeriod(AnalysisOptionDto dto) {
        return adminMainMapper.listUserChargeRankByPeriod(dto);
    }

    public String getUserResignCheck(String id) {
        return adminMainMapper.getUserResignCheck(id);
    }

    public String getGroupManagerCheck(String id) {
        return adminMainMapper.getGroupManagerCheck(id);
    }




}

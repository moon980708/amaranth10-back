package com.douzone.amaranth10.domain.Login.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class LoginInfoDto {
    private String id;
    private String userName;
    private String rankName;
    private String deptName;

}

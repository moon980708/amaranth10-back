package com.douzone.amaranth10.domain.Login.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class InputLoginDto {
    @NotNull(message = "id은 null이 될 수 없습니다.")
    @Size(min = 1,message = "id는 한글자 이상이여야 합니다.")
    private String id;

    @NotNull(message = "password은 null이 될 수 없습니다.")
    @Size(min = 1,message = "password는 한글자 이상이여야 합니다.")
    private String password;

}

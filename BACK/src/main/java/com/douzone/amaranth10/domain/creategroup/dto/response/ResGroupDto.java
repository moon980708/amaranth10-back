package com.douzone.amaranth10.domain.creategroup.dto.response;

import lombok.Data;

@Data
public class ResGroupDto {
    private int groupNo;
    private String groupName;
    private boolean usable;
    private int count;

}

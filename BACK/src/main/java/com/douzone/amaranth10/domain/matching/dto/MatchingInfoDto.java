package com.douzone.amaranth10.domain.matching.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class MatchingInfoDto {
    private Integer categoryNo;
    private List<Integer> itemNoList;
    private List<Integer> itemOptionList;
    private List<Boolean> itemEssentialList;
}

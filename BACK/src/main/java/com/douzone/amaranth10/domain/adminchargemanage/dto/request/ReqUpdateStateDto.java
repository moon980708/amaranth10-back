package com.douzone.amaranth10.domain.adminchargemanage.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class ReqUpdateStateDto {
    private String managerId;

    //승인 or 반려
    private String state;

    private List<Integer> userChargeNoList;

    //반려 사유
    private String rejectReason;
}

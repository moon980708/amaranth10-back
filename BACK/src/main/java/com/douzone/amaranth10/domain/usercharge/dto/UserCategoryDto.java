package com.douzone.amaranth10.domain.usercharge.dto;

import lombok.Data;

@Data
public class UserCategoryDto {
    private int categoryNo;
    private String categoryName;
}

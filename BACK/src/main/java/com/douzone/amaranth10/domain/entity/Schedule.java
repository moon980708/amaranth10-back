package com.douzone.amaranth10.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@NoArgsConstructor
@Getter
@Setter
public class Schedule {
    private int scheduleNo;

    private Timestamp yearMonth;
    private int schedule;
    private Timestamp chargeStart;
    private Timestamp chargeEnd;
    private String close;
    private Timestamp expendStart;
    private Timestamp expendEnd;
    private Timestamp payday;

}

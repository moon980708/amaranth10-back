package com.douzone.amaranth10.domain.usermain.dto;

import lombok.*;

@Data
public class MonthlyChartDto {
    private int year;
    private int month;
    private String categoryName;
    private Long totalCharge;
}
package com.douzone.amaranth10.domain.userchargesearch.dao;

import com.douzone.amaranth10.domain.usercharge.dto.UserChargeDto;
import com.douzone.amaranth10.domain.userchargesearch.dao.mapper.UserChargeSearchMapper;
import com.douzone.amaranth10.domain.userchargesearch.dto.UserChargeSearchDto;
import com.douzone.amaranth10.domain.userchargesearch.dto.UserSearchOptionDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
@RequiredArgsConstructor
public class UserChargeSearchDao {
    private final UserChargeSearchMapper userChargeSearchMapper;

    public List<UserChargeSearchDto> userChargeSearchList(UserSearchOptionDto userSearchOptionDto) {
        return userChargeSearchMapper.userChargeSearchList(userSearchOptionDto);
    }

    public List<Integer> getYearMenuItem(String id) {
        return userChargeSearchMapper.getYearMenuItem(id);
    }

    public UserChargeSearchDto userChargeLogDetail(long no) {
        return userChargeSearchMapper.userChargeLogDetail(no);
    }

    public void userRecharge(int userChargeNo) {
        userChargeSearchMapper.userRecharge(userChargeNo);
    }
}

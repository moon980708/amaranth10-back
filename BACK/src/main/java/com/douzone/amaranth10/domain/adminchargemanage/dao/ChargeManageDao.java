package com.douzone.amaranth10.domain.adminchargemanage.dao;

import com.douzone.amaranth10.domain.adminchargemanage.dao.mapper.ChargeManageMapper;
import com.douzone.amaranth10.domain.adminchargemanage.dto.response.ResChargeDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqSearchChargeOptionDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqSearchChargeUserOptionDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqUpdateStateDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.response.ChargeUserDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.response.ItemDto;
import com.douzone.amaranth10.domain.entity.Schedule;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
@RequiredArgsConstructor
public class ChargeManageDao {
    private final ChargeManageMapper chargeManageMapper;

    public List<Schedule> listSchedule() {
        return chargeManageMapper.listSchedule();
    }

    public List<ChargeUserDto> listSearchUserCharge(ReqSearchChargeUserOptionDto dto) {
        return chargeManageMapper.listSearchUserCharge(dto);
    }

    public List<ResChargeDto> listCharge(ReqSearchChargeOptionDto dto) {
        return chargeManageMapper.listCharge(dto);
    }

    public ResChargeDto getChargeDetail(int userChargeNo) {
        return chargeManageMapper.getChargeDetail(userChargeNo);
    }

    public List<ItemDto> getChargeDetailItem(int userChargeNo) {
        return chargeManageMapper.getChargeDetailItem(userChargeNo);
    }

    public int checkManageableCharge(ReqUpdateStateDto dto) {
        return chargeManageMapper.checkManageableCharge(dto);
    }
    public int checkNowWaitCharge(ReqUpdateStateDto dto) {
        return chargeManageMapper.checkNowWaitCharge(dto);
    }

    public void UpdateStateUserCharge(ReqUpdateStateDto dto) {
        chargeManageMapper.UpdateStateUserCharge(dto);
    }



    //scheduler
    public Integer findScheduleNoByPaydayOver() {
        return chargeManageMapper.findScheduleNoByPaydayOver();
    }

    public void updateRejectByPaydayOver(Integer scheduleNo) {
        chargeManageMapper.updateRejectByPaydayOver(scheduleNo);
    }
}

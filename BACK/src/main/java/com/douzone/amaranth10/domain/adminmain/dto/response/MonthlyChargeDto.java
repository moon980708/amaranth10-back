package com.douzone.amaranth10.domain.adminmain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MonthlyChargeDto {
    private String chargeMonth;
    private int count;
    private long sum;
}

package com.douzone.amaranth10.domain.adminchargemanage.application;

import com.douzone.amaranth10.domain.adminchargemanage.dao.ChargeManageDao;
import com.douzone.amaranth10.domain.adminchargemanage.dto.response.ChargeUserDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqSearchChargeOptionDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqSearchChargeUserOptionDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqUpdateStateDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.response.ResChargeDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.response.ResSummaryChargeUserDto;
import com.douzone.amaranth10.domain.entity.Schedule;
import com.douzone.amaranth10.global.exception.BusinessException;
import com.douzone.amaranth10.global.exception.ErrorCode;
import com.douzone.amaranth10.global.util.EncryptorsUtils;
import com.douzone.amaranth10.global.util.PaginationUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ChargeManageService {

    private final ChargeManageDao chargeManageDao;
    private final BCryptPasswordEncoder passwordEncoder;


    public List<Schedule> listSchedule() {
        return chargeManageDao.listSchedule();
    }

    public ResSummaryChargeUserDto summaryChargeUser(ReqSearchChargeUserOptionDto dto) {
        List<ChargeUserDto> searchUserCharge = chargeManageDao.listSearchUserCharge(dto);
        System.out.println("검색결과!!!!!!!!!:"+ searchUserCharge);

        return ResSummaryChargeUserDto.builder()
                .total(getTotal(searchUserCharge))
                .user(searchUserCharge)
                .build();
    }

    public List<ResChargeDto> listCharge(ReqSearchChargeOptionDto dto) {
        //limit offset을 위해 start 얻기
        int start = PaginationUtil.getLimitOffset(dto.getPage(), dto.getRowsPerPage());
        dto.setStart(start);
        System.out.println("페이징을 위한 dto"+dto);

        return chargeManageDao.listCharge(dto);
    }

    public ResChargeDto getChargeDetail(int userChargeNo) {
        ResChargeDto charge = chargeManageDao.getChargeDetail(userChargeNo);
        String decryptedEmail = EncryptorsUtils.decrypt(charge.getEmail());
        charge.setEmail(decryptedEmail);    //이메일 복호화시켜서 다시 set해주기
        charge.setItemList(chargeManageDao.getChargeDetailItem(userChargeNo));    //항목리스트넣어주기

        return charge;
    }

    @Transactional
    public String updateStateUserCharge (ReqUpdateStateDto dto) {
        int tryUpdateChargeCount = dto.getUserChargeNoList().size();  //요청건수
        int manageableChargeCount = chargeManageDao.checkManageableCharge(dto);  //요청건수 중 관리가능

        if(tryUpdateChargeCount != manageableChargeCount) {
            System.out.println("요청건수:"+tryUpdateChargeCount+"/ 관리가능건수:"+manageableChargeCount);
            throw new BusinessException(ErrorCode.UNMANAGEABLEPROCESSED_ERROR);
        }

        int nowWaitChargeCount = chargeManageDao.checkNowWaitCharge(dto);

        if(nowWaitChargeCount == 0) {
            throw new BusinessException(ErrorCode.ALREADYPROCESSED_ERROR);
        }

        chargeManageDao.UpdateStateUserCharge(dto);

        if(manageableChargeCount != nowWaitChargeCount) {
            int alreadyProcessedChargeCount = manageableChargeCount - nowWaitChargeCount;
            return "이미 처리된 청구 "+ alreadyProcessedChargeCount +"건을 제외한, 총 "+ nowWaitChargeCount +"건이 처리되었습니다.";
        }

        return "";
    }


    //scheduler
    //매일 자정, 지급일이 되었다면 승인대기인 청구건들은 자동으로 반려처리.
    @Scheduled(cron = "0 0 0 * * ?")
    public void updateRejectByPaydayOverScheduler() {
        Integer scheduleNo = chargeManageDao.findScheduleNoByPaydayOver();
        System.out.println("지급일 당일이 되었기에 반려처리하려는 차수: "+scheduleNo);

        if(scheduleNo != null) {
            chargeManageDao.updateRejectByPaydayOver(scheduleNo);    //해당 차수 승인대기건을 반려로 변경
        }
    }





    //private method
    private ChargeUserDto getTotal(List<ChargeUserDto> queryResult) {
        int count = 0;
        long sum = 0;

        if(queryResult.size() == 0) {
            return null;
        }


        for (ChargeUserDto dto : queryResult) {
            count += dto.getCount();
            sum += dto.getSum();
        }

        return ChargeUserDto.builder()
                .id("total")
                .userName("전체")
                .count(count)
                .sum(sum)
                .searchTime(queryResult.get(0).getSearchTime())
                .build();
    }

}

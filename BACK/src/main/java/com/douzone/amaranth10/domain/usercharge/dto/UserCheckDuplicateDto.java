package com.douzone.amaranth10.domain.usercharge.dto;


import lombok.Data;

@Data
public class UserCheckDuplicateDto {
    private String id;
    private String expendDate;
    private long userChargeNo;

}

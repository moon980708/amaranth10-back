package com.douzone.amaranth10.domain.userchargesearch.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UserChargeDetailDto {
    private long userChargeNo;
    private int itemNo;
    private String itemTitle;
    private String value;
}

package com.douzone.amaranth10.domain.usermain.dto;

import lombok.*;

@Data
public class CategoryChartDto {
    private String categoryName;
    private Long totalCharge;
    private Integer year;
    private Integer month;
}
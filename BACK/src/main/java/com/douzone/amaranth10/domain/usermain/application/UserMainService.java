package com.douzone.amaranth10.domain.usermain.application;

import com.douzone.amaranth10.domain.usermain.dao.UserMainDao;
import com.douzone.amaranth10.domain.usermain.dto.*;
import com.douzone.amaranth10.domain.usermain.dto.UserInfoModifyDto;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.douzone.amaranth10.global.util.EncryptorsUtils;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserMainService {

    private final UserMainDao userMainDao;
    private final BCryptPasswordEncoder passwordEncoder;

    public UserInfoDto userInfo(String id) {
        UserInfoDto userInfoDto = userMainDao.userInfo(id);
        String decryptedEmail = EncryptorsUtils.decrypt(userInfoDto.getEmail());
        userInfoDto.setEmail(decryptedEmail);
        return userInfoDto;
    }

    @Transactional
    public void userInfoUpdate(UserInfoModifyDto userInfoModifyDto) {
        String inputPass = userInfoModifyDto.getPassword();
        String encryptedPassword = passwordEncoder.encode(userInfoModifyDto.getPassword());
        String encryptedEmail = EncryptorsUtils.encrypt(userInfoModifyDto.getEmail());
        String dtoId = userInfoModifyDto.getId();
        String getpass = userMainDao.getPassword(dtoId);

        if(userInfoModifyDto.getPassword().isEmpty()|| passwordEncoder.matches(inputPass, userMainDao.getPassword(userInfoModifyDto.getId())) == true) {

            UserUpdateEmailDto emailDto = new UserUpdateEmailDto();
            emailDto.setEmail(encryptedEmail);
            emailDto.setId(userInfoModifyDto.getId());
            userMainDao.userInfoEmailUpdate(emailDto);

        }else {
//            userInfoModifyDto.setEmail(encryptedEmail);
            UserUpdateEmailDto emailDto = new UserUpdateEmailDto();
            UserUpdatePasswordDto passwordDto = new UserUpdatePasswordDto();

            emailDto.setEmail(encryptedEmail);
            emailDto.setId(userInfoModifyDto.getId());

            passwordDto.setId(userInfoModifyDto.getId());
            passwordDto.setPassword(encryptedPassword);
            userMainDao.userInfoPasswordUpdate(passwordDto);
            userMainDao.userInfoEmailUpdate(emailDto);
        }
    }

    public List<ChargeSummaryDto> userChargeSummary(String id) {
        List<ChargeSummaryDto> chargeSummaryDto = userMainDao.userChargeSummary(id);
        return chargeSummaryDto;
    }

    public List<CategoryChartDto> userCategoryChart(String id, int year, int month) {
        List<CategoryChartDto> categoryChartDto = userMainDao.userCategoryChart(id, year, month);
        return categoryChartDto;
    }

    public List<MonthlyChartDto> userMonthlyChart(String id) {
        List<MonthlyChartDto> monthlyChartDto = userMainDao.userMonthlyChart(id);
        return monthlyChartDto;
    }
}

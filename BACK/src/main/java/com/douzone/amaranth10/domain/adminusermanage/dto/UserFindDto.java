package com.douzone.amaranth10.domain.adminusermanage.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class UserFindDto {
    private Integer deptNo;
    private Integer rankNo;
    private Boolean resign;
    private String keyword;
    private Integer offset;
}

package com.douzone.amaranth10.domain.creategroup.dto.response;

import lombok.Data;

@Data
public class ResDeptUserDto {
    private String id;
    private String userName;
    private int deptNo;
    private boolean resign;

    private String deptName;
    private String rankName;

}

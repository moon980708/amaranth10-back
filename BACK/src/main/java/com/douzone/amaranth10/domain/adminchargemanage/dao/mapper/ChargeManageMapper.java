package com.douzone.amaranth10.domain.adminchargemanage.dao.mapper;

import com.douzone.amaranth10.domain.adminchargemanage.dto.response.ResChargeDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqSearchChargeOptionDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqSearchChargeUserOptionDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.request.ReqUpdateStateDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.response.ChargeUserDto;
import com.douzone.amaranth10.domain.adminchargemanage.dto.response.ItemDto;
import com.douzone.amaranth10.domain.entity.Schedule;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ChargeManageMapper {
    public List<Schedule> listSchedule();
    public List<ChargeUserDto> listSearchUserCharge(ReqSearchChargeUserOptionDto dto);
    public List<ResChargeDto> listCharge(ReqSearchChargeOptionDto dto);
    public ResChargeDto getChargeDetail(int userChargeNo);
    public List<ItemDto> getChargeDetailItem(int userChargeNo);


    public int checkManageableCharge(ReqUpdateStateDto dto);
    public int checkNowWaitCharge(ReqUpdateStateDto dto);
    public void UpdateStateUserCharge(ReqUpdateStateDto dto);


    public Integer findScheduleNoByPaydayOver();
    public void updateRejectByPaydayOver(Integer scheduleNo);


}

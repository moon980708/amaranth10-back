package com.douzone.amaranth10.domain.creategroup.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReqGroupAndManagerDto {
    private int groupNo;

    private List<String> insertList;
    private List<String> deleteList;
}

package com.douzone.amaranth10.domain.createcategoryanditem.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class ItemDto {
    private int itemNo;
    private String itemTitle;
    private String itemType;
}

package com.douzone.amaranth10.domain.adminmain.dto.response;

import lombok.Data;

import java.math.BigInteger;

@Data
public class BaseAnalysisDto {
    private String name;
    private int count;
    private BigInteger sum;
}

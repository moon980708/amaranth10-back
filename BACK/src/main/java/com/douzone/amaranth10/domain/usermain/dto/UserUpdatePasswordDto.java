package com.douzone.amaranth10.domain.usermain.dto;

import lombok.Data;

@Data
public class UserUpdatePasswordDto {
    private String id;
    private String password;
}

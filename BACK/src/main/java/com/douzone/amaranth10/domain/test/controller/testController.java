package com.douzone.amaranth10.domain.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin("*")
public class testController {
    @Autowired
    private com.douzone.amaranth10.domain.test.application.testService testService;
    @GetMapping("/test")
    public String userName(){
        System.out.println("testName success");
        return testService.userName();

    }
}

package com.douzone.amaranth10.domain.adminmain.controller;

import com.douzone.amaranth10.domain.adminmain.dto.request.ReqMonthDeptDto;
import com.douzone.amaranth10.domain.adminmain.dto.request.ReqMonthScheduleDto;
import com.douzone.amaranth10.domain.adminmain.application.AdminMainService;
import com.douzone.amaranth10.domain.adminmain.dto.request.ReqAnalysisOptionDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.ResAnalysisByGroupDeptByPeriodDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.ResConfirmStaticsDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.ResGroupDeptDto;
import com.douzone.amaranth10.domain.adminmain.dto.response.ResWaitDto;
import com.douzone.amaranth10.domain.entity.Schedule;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/admin/main")
public class AdminMainController {
    private final AdminMainService adminMainService;

    @GetMapping("/waitlist")
    public ResWaitDto listWait(@RequestParam String managerId) {
        //현재 승인대기 내역 불러오기
        System.out.println("승인대기 내역:"+managerId);
        return adminMainService.listWait(managerId);
    }

    @PostMapping("/monthdept")
    public ResConfirmStaticsDto listMonthDept(@RequestBody ReqMonthDeptDto dto) {
        //해당 청구년월의 best5 승인금액/건수 + 총 승인금액/건수
        System.out.println("청구년월:"+dto);
        return adminMainService.getDeptConfirmByMonth(dto);
    }

    @PostMapping("/schedulelist")
    public List<Schedule> listMonthSchedule(@RequestBody ReqMonthScheduleDto dto) {
        //달력에 띄울 해당 청구년월의 차수리스트
        System.out.println(dto);
        return adminMainService.listMonthSchedule(dto);
    }

    @GetMapping("/groupdeptlist")
    public ResGroupDeptDto listGroupDept(@RequestParam String managerId) {
        //메뉴에 띄울 사용중인 취합그룹+부서 리스트
        return adminMainService.listGroupDept(managerId);
    }

    @PostMapping("/analysis")
    public ResAnalysisByGroupDeptByPeriodDto listAnalysis(@RequestBody ReqAnalysisOptionDto dto) {
        //선택한 취합그룹/부서 + 조회기간에 따른 경비현황분석 결과
        System.out.println("경비현황분석: "+dto);
        return adminMainService.listAnalysis(dto);
    }





}

package com.douzone.amaranth10.domain.adminmain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MonthDeptDto {
    private String deptName;
    private int countChargeDept;
    private BigInteger sumChargeDept;
}
package com.douzone.amaranth10.domain.usermain.controller;

import com.douzone.amaranth10.domain.Login.application.LoginService;
import com.douzone.amaranth10.domain.usermain.dto.*;
import com.douzone.amaranth10.domain.usermain.dto.UserInfoDto;
import com.douzone.amaranth10.domain.usermain.application.UserMainService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Slf4j
public class UserMainController {

    private final UserMainService userMainService;
    private final LoginService loginService;

    @GetMapping("/myinfo")
    public UserInfoDto userInfo(
            @RequestParam String id
    ) {
        UserInfoDto userInfoDto = userMainService.userInfo(id);
        return userInfoDto;
    }

    @GetMapping("/myinfo/passcheck")
    public String passCheck(
            @RequestParam String id,
            @RequestParam String password
    ) throws IOException {
        String result = loginService.realLogin(id, password);

        if (result.equals("loginFail")) {
            return "loginFail";
        } else {
            return "loginSuccess";
        }
    }

    @PostMapping("/myinfo")
    public void userInfoUpdate(
            @RequestBody UserInfoModifyDto userInfoModifyDto
    ) {
        log.error("data: {}", userInfoModifyDto);
        userMainService.userInfoUpdate(userInfoModifyDto);
    }

    @GetMapping("/home/summary")
    public List<ChargeSummaryDto> userChargeSummary(
            @RequestParam String id

    ) {
        return userMainService.userChargeSummary(id);
    }

    @GetMapping("/home/categorychart")
    public List<CategoryChartDto> userCategoryChart(
            @RequestParam String id,
            @RequestParam int year,
            @RequestParam int month
    ) {

        return userMainService.userCategoryChart(id, year, month);
    }

    @GetMapping("/home/monthlychart")
    public List<MonthlyChartDto> userMonthlyChart(
            @RequestParam String id
    ) {
        return userMainService.userMonthlyChart(id);
    }


}

package com.douzone.amaranth10.domain.adminusermanage.application;

import com.douzone.amaranth10.domain.adminusermanage.dao.UserManageDao;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserAddDto;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserFindDto;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserManageDto;
import com.douzone.amaranth10.domain.adminusermanage.dto.UserSaveDto;
import com.douzone.amaranth10.domain.creategroup.dao.GroupDao;
import com.douzone.amaranth10.global.util.EncryptorsUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class UserManageService {
    private final UserManageDao userManageDao;

    private final BCryptPasswordEncoder passwordEncoder;

    private final GroupDao groupDao;

    public List<UserManageDto> userList(Integer page) {
        Integer newOffset = (page - 1) * 10;

        List<UserManageDto> userList = userManageDao.userList(newOffset);

        for (UserManageDto user : userList) {
            String decryptedEmail = EncryptorsUtils.decrypt(user.getEmail());
//            System.out.println(decryptedEmail); // 복호화 된 이메일
            user.setEmail(decryptedEmail);
        }
        return userList;
    }

    public Integer getSearchCount(HashMap<String, Object> map) {
        return userManageDao.getSearchCount(map);
    }

    public List<String> userDept() {
        return userManageDao.userDept();
    }

    public List<String> userRank() {
        return userManageDao.userRank();
    }

    public List<UserManageDto> findUser(Integer page, HashMap<String, Object> map) {
        Integer newOffset = (page - 1) * 10;

        Integer deptNo = (Integer) map.get("deptNo");
        Integer rankNo = (Integer) map.get("rankNo");
        Boolean resign = (Boolean) map.get("resign");
        String keyword = (String) map.get("keyword");

        UserFindDto dto = new UserFindDto();
        dto.setDeptNo(deptNo);
        dto.setRankNo(rankNo);
        dto.setResign(resign);
        dto.setKeyword(keyword);
        dto.setOffset(newOffset);

        List<UserManageDto> findUser = userManageDao.findUser(dto);

        for (UserManageDto user : findUser) {
            String decryptedEmail = EncryptorsUtils.decrypt(user.getEmail());
//            System.out.println(decryptedEmail); // 복호화 된 이메일
            user.setEmail(decryptedEmail);
        }
        return findUser;
    }

    @Transactional
    public List<UserManageDto> saveUser(HashMap<String, Object> map) {

        Integer deptNo = (Integer) map.get("deptNo");
        Integer rankNo = (Integer) map.get("rankNo");
        Boolean resign = (Boolean) map.get("resign");
        String keyword = (String) map.get("keyword");

        UserSaveDto dto = new UserSaveDto();
        dto.setDeptNo(deptNo);
        dto.setRankNo(rankNo);
        dto.setResign(resign);
        dto.setKeyword(keyword);

        List<UserManageDto> saveUser = userManageDao.saveUser(dto);

        for (UserManageDto user : saveUser) {
            String decryptedEmail = EncryptorsUtils.decrypt(user.getEmail());
//            System.out.println(decryptedEmail); // 복호화 된 이메일
            user.setEmail(decryptedEmail);
        }
        return saveUser;
    }

    public void insertUser(UserAddDto userAddDto) {
        String encryptedPassword = passwordEncoder.encode(userAddDto.getPassword()); // dto에서 가져온 패스워드 암호화(복호화 x)
        String encryptedEmail = EncryptorsUtils.encrypt(userAddDto.getEmail()); // dto에서 가져온 이메일 암호화

        userAddDto.setPassword(encryptedPassword);
        userAddDto.setEmail(encryptedEmail);

        userManageDao.insertUser(userAddDto);
    }

    @Transactional
    public void updateUser(HashMap<String, Object> updateMap) {
        String encryptedEmail = EncryptorsUtils.encrypt((String) updateMap.get("email"));

        HashMap<String, Object> encryptedUpdateMap = new HashMap<>(updateMap);   // 기존 Controller의 HashMap 복사
        encryptedUpdateMap.put("email", encryptedEmail);   // 새로운 이메일 암호화해서 추가

        userManageDao.updateUser(encryptedUpdateMap);
    }

    @Transactional
    public void updateResign(HashMap<String, Object> updateMap) {
        //퇴사하면서, 그룹담당자였다면 권한없애기
        String id = (String) updateMap.get("id");
        groupDao.deleteResignGroupManager(id);

        userManageDao.updateResign(updateMap);
    }
}

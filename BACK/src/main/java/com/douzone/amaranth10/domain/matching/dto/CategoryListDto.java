package com.douzone.amaranth10.domain.matching.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class CategoryListDto {
    private int categoryNo;
    private String categoryName;
    private boolean usable;
}

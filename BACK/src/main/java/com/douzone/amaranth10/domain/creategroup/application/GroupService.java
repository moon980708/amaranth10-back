package com.douzone.amaranth10.domain.creategroup.application;

import com.douzone.amaranth10.domain.creategroup.dao.GroupDao;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqGroupAndManagerDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqSearchGroupUserOptionDto;
import com.douzone.amaranth10.domain.creategroup.dto.response.ResDeptUserDto;
import com.douzone.amaranth10.domain.creategroup.dto.response.ResGroupDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqGroupAndMemberDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqSearchGroupOptionDto;
import com.douzone.amaranth10.domain.entity.Group;
import com.douzone.amaranth10.global.exception.BusinessException;
import com.douzone.amaranth10.global.exception.ErrorCode;
import com.douzone.amaranth10.global.util.PaginationUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GroupService {

    private final GroupDao groupDao;

    public List<ResGroupDto> listGroup(ReqSearchGroupOptionDto dto) {
        List<ResGroupDto> groupList = groupDao.listGroup(dto);
        return groupList;
    };

    public List<ResDeptUserDto> listGroupMember(ReqSearchGroupUserOptionDto dto) {

        int page = dto.getPage();
        int rowsPerPage = dto.getRowsPerPage();

        if (rowsPerPage != 0) {
            dto.setStart(PaginationUtil.getLimitOffset(page, rowsPerPage));
        }

        System.out.println(dto);
        return groupDao.listGroupMember(dto);
    }

    public List<ResDeptUserDto> listGroupManager(ReqSearchGroupUserOptionDto dto) {
        return groupDao.listGroupManager(dto);
    }

    public List<ResDeptUserDto> listDeptUser() {
        return groupDao.listDeptUser();
    }

    @Transactional
    public int insertGroup(ReqGroupAndMemberDto dto){
        groupDao.insertGroup(dto);
        System.out.println("방금 생성한 그룹번호까지 포함:"+dto);

        int groupNo = dto.getGroupNo();

        groupDao.insertGroupMember(dto);

        groupDao.insertGroupManager(ReqGroupAndManagerDto.builder()
                        .groupNo(groupNo)
                        .insertList(Arrays.asList("admin"))
                        .build());

        return groupNo;
    }

    @Transactional
    public void editGroupMember(ReqGroupAndMemberDto dto) {
        //지우고 나서 새로 넣어주기 -> 수정
        List<String> deleteList = dto.getDeleteList();
        List<String> insertList = dto.getInsertList();

        if(deleteList.size() > 0) {     //delete배열 존재할때만 삭제
            groupDao.deleteGroupMember(dto);
        }

        if(insertList.size() > 0) {     //insert배열 존재할때만 새로 넣어주기
            groupDao.insertGroupMember(dto);
        }

    }

    @Transactional
    public void editGroupManager(ReqGroupAndManagerDto dto) {
        //지우고 나서 새로 넣어주기 -> 수정
        List<String> deleteList = dto.getDeleteList();
        List<String> insertList = dto.getInsertList();

        if(deleteList.size() > 0) {     //delete배열 존재할때만 삭제
            groupDao.deleteGroupManager(dto);
        }

        if(insertList.size() > 0) {     //insert배열 존재할때만 새로 넣어주기
            if(groupDao.checkBeforeUpdateGroupManager(insertList) != 0) {     //퇴사자 포함
                throw new BusinessException(ErrorCode.INCLUDERESIGNED_ERROR);
            }
            groupDao.insertGroupManager(dto);
        }

    }

    @Transactional
    public void deleteGroupMember(ReqGroupAndMemberDto dto) {
        groupDao.deleteGroupMember(dto);
    }

    @Transactional
    public void deleteGroupManager(ReqGroupAndManagerDto dto) {
        groupDao.deleteGroupManager(dto);
    }

    public void editGroup(Group dto) {
        groupDao.updateGroup(dto);
    }

    @Transactional
    public void deleteGroup(int groupNo) {
        groupDao.deleteGroup(groupNo);

        //그룹멤버도 지워야함.
        groupDao.deleteGroupMember(
                ReqGroupAndMemberDto.builder()
                .groupNo(groupNo)
                .build());

        //그룹담당자도 지워야함.
        groupDao.deleteGroupManager(
                ReqGroupAndManagerDto.builder()
                        .groupNo(groupNo)
                        .build());
    }



}

package com.douzone.amaranth10.domain.usermain.dto;

import lombok.*;

@Data
public class UserInfoModifyDto {
    private String id;
    private String password;
    private String email;
}


package com.douzone.amaranth10.domain.usercharge.dto;


import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.sql.Date;

@Data
public class UserScheduleViewDto {
    private int scheduleNo;
    private int year;
    private int month;
    private int schedule;
    private Date chargeStart;
    private Date chargeEnd;
    private String close;
    private Date expendStart;
    private Date expendEnd;
}

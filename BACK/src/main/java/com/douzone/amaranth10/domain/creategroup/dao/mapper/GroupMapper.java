package com.douzone.amaranth10.domain.creategroup.dao.mapper;

import com.douzone.amaranth10.domain.creategroup.dto.request.ReqGroupAndManagerDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqSearchGroupOptionDto;
import com.douzone.amaranth10.domain.creategroup.dto.response.ResDeptUserDto;
import com.douzone.amaranth10.domain.creategroup.dto.response.ResGroupDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqSearchGroupUserOptionDto;
import com.douzone.amaranth10.domain.creategroup.dto.request.ReqGroupAndMemberDto;
import com.douzone.amaranth10.domain.entity.Group;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GroupMapper {
    public List<ResGroupDto> listGroup(ReqSearchGroupOptionDto dto);
    public List<ResDeptUserDto> listGroupMember(ReqSearchGroupUserOptionDto dto);

    public List<ResDeptUserDto> listGroupManager(ReqSearchGroupUserOptionDto dto);
    public List<ResDeptUserDto> listDeptUser();
    public void insertGroup(ReqGroupAndMemberDto dto);


    public void insertGroupMember(ReqGroupAndMemberDto dto);
    public void deleteGroupMember(ReqGroupAndMemberDto dto);
    public void updateGroup(Group dto);
    public void deleteGroup(int groupNo);

    public int checkBeforeUpdateGroupManager(List<String> insertList);
    public void insertGroupManager(ReqGroupAndManagerDto dto);
    public void deleteGroupManager(ReqGroupAndManagerDto dto);

    public void deleteResignGroupManager(String managerId);
}

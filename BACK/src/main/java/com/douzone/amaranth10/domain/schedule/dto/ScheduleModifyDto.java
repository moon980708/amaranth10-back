package com.douzone.amaranth10.domain.schedule.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class ScheduleModifyDto {

    private int scheduleNo;
    private LocalDateTime chargeStart;
    private LocalDateTime chargeEnd;
    private LocalDateTime expendStart;
    private LocalDateTime expendEnd;
    private LocalDateTime payday;

}

package com.douzone.amaranth10.domain.userchargesearch.dto;

import lombok.Data;

@Data
public class UserSearchOptionDto {
    private String id;
    private String searchValue;
    private String searchField;
    private int year;
    private int month;
    private String state;
}

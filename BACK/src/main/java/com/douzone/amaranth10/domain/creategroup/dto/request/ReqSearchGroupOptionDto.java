package com.douzone.amaranth10.domain.creategroup.dto.request;

import lombok.Data;

@Data
public class ReqSearchGroupOptionDto {
    private String searchKeyword;
    private String searchUsable;
}

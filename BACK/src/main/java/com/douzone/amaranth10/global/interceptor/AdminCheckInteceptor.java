package com.douzone.amaranth10.global.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AdminCheckInteceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HttpSession session = request.getSession(false);
        String id = (String) session.getAttribute("session");   //세션에 저장된 id

        if (!id.equals("admin")) {
            System.out.println("관리자 권한 없음");
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);    //403 에러
            return false;
        }

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }
}

package com.douzone.amaranth10.global.session;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionCheckInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HttpSession session = request.getSession(false);
        System.out.println("이동 전 세션 체크 session = " + session);

        if(session == null){
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // 401 Unauthorized 처리
//            response.getWriter().write("Session expired");
            return false;
        }

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }
}

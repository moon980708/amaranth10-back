package com.douzone.amaranth10.global.exception;

import com.douzone.amaranth10.global.aspect.ExecutionTimeAspect;
import com.douzone.amaranth10.global.response.CommonResponse;
import com.douzone.amaranth10.global.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.rmi.AlreadyBoundException;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ExecutionTimeAspect.class);

    @ExceptionHandler({
            NoHandlerFoundException.class
    })
    public ResponseEntity<CommonResponse<String>> notFoundExceptionHandle(final NoHandlerFoundException e) {

        String errorMessage = String.format("!!!!notFoundExceptionHandle!!!! [Requested URL: %s], [HTTP Method: %s]",
                e.getRequestURL(),
                e.getHttpMethod());
        logger.error(errorMessage, e.getMessage());

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .contentType(MediaType.APPLICATION_JSON)
                .body(CommonResponse.error(new ErrorResponse("잘못된 주소 요청")));
    }

    //런타임시, 예외처리 (비즈니스 예외처리)
    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<CommonResponse<String>> businessException(final BusinessException e) {
        logger.error("custom error :\n" + e.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(CommonResponse.error(new ErrorResponse(e.getMessage())));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<CommonResponse<String>> handleNotValidException(
            final MethodArgumentNotValidException e) {
        StringBuilder errors = new StringBuilder();
        e.getBindingResult().getFieldErrors().forEach(error -> {
            String field = error.getField();
            String defaultMessage = error.getDefaultMessage();
            errors.append(field).append(": ").append(defaultMessage).append("\n");
        });

        logger.error("!!!!MethodArgumentNotValidException!!!! in method " + e.getParameter().getMethod().getName() + "\n" + errors.toString());


        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(CommonResponse.error(new ErrorResponse("매개변수 유효하지 않음")));
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<CommonResponse<String>> handleException(final Exception e) {

        String errorMessage = String.format("!!!!handleException!!!! Type: %s, Message: %s",
                e.getClass().getName(),
                e.getMessage());

        logger.error(errorMessage);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON)
                .body(CommonResponse.error(new ErrorResponse("에러 발생")));
    }
}
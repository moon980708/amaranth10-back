package com.douzone.amaranth10.global.util;

public class PaginationUtil {
    public static int getLimitOffset(int page, int rowsPerPage) {
        //mui pagination 이용시 1페이지의 page값 : 0
        //원래라면 : (page-1)*rowsPerpage , (page*rowsPerPage)-1
        //mui라면 위의 공식에서 page : page+1 해줘야함
        //return start
        return (page*rowsPerPage);
    }
}

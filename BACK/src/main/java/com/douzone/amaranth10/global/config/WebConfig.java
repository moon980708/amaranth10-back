package com.douzone.amaranth10.global.config;

import com.douzone.amaranth10.domain.adminmain.application.AdminMainService;
import com.douzone.amaranth10.global.interceptor.AdminCheckInteceptor;
import com.douzone.amaranth10.global.interceptor.GroupManagerCheckInteceptor;
import com.douzone.amaranth10.global.interceptor.UserResignCheckInteceptor;
import com.douzone.amaranth10.global.session.SessionCheckInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@RequiredArgsConstructor
@Configuration
//@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    private final AdminMainService adminMainService;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:3000")
                .allowedMethods("GET", "POST", "PUT", "DELETE")
                .allowCredentials(true)
                .allowedHeaders("*");
    }

    @Override
    public  void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(new SessionCheckInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/realLogin", "/sessionCheck");

        registry.addInterceptor(new UserResignCheckInteceptor(adminMainService))
                .addPathPatterns("/**")
                .excludePathPatterns("/realLogin", "/sessionCheck");

        registry.addInterceptor(new AdminCheckInteceptor())
                .addPathPatterns("/admin/**")
                .excludePathPatterns("/admin/main/**", "/admin/charge/**");

        registry.addInterceptor(new GroupManagerCheckInteceptor(adminMainService))
                .addPathPatterns("/admin/main/**", "/admin/charge/**");
    }
}

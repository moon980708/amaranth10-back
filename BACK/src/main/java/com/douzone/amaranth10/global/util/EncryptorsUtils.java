package com.douzone.amaranth10.global.util;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class EncryptorsUtils {
//    private static final String PASSWORD = "secret";
//    private static final String SALT = "5c0744940b5c369b"; // 이거는 예시

//    private static TextEncryptor textEncryptor = Encryptors.text(PASSWORD, SALT);

    private static String PASSWORD;
    private static String SALT;

    private static TextEncryptor textEncryptor;

    private final RedisTemplate<String, String> redisTemplate;

    public EncryptorsUtils(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    public void init() {
        // Redis에서 PASSWORD와 SALT 가져옴
        PASSWORD = redisTemplate.opsForValue().get("secret");
        SALT = redisTemplate.opsForValue().get("salt");

        // TextEncryptor 초기화
        textEncryptor = Encryptors.text(PASSWORD, SALT);
    }
    public static String encrypt(String data) {
        return textEncryptor.encrypt(data);
    }

    public static String decrypt(String encryptedData) {
        return textEncryptor.decrypt(encryptedData);
    }
}

package com.douzone.amaranth10.global.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
public class BusinessException extends RuntimeException {
    private final ErrorCode errorCode;

    public BusinessException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public BusinessException(ErrorCode errorCode, String... args) {
        // 가변인자를 사용하여 동적인 메시지 전달 가능하도록
        super(String.format(errorCode.getMessage(), (Object[]) args));
        this.errorCode = errorCode;
    }

}

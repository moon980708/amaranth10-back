package com.douzone.amaranth10.global.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;


@Component
@Aspect
public class ExecutionTimeAspect {

    private static final Logger logger = LoggerFactory.getLogger(ExecutionTimeAspect.class);

    @Around("execution(* *..*.controller.*.*(..))")	/* com.douzone.mysite.repository.class명.function명(parameter) */
    public Object adviceAround(ProceedingJoinPoint pjp) throws Throwable {

        /* Before */
        StopWatch sw = new StopWatch();
        sw.start();

        Object result = pjp.proceed();

        /* After */
        sw.stop();
        Long totalTime = sw.getTotalTimeMillis();
        String className = pjp.getTarget().getClass().getName();
        className = className.substring(className.lastIndexOf('.') + 1);

        String methodName = pjp.getSignature().getName();
        String taskName = className + "." + methodName;

//        System.out.println("=====[Exection Time] [" + taskName + "] - " + totalTime + " milis=====");

        logger.info("=====[실행시간] [" + taskName + "] - " + totalTime + " milis=====");
        return result;


    }
}

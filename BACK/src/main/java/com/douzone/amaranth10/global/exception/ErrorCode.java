package com.douzone.amaranth10.global.exception;


public enum ErrorCode {

    UNMANAGEABLEPROCESSED_ERROR("UnmanageableProcessedError", "관리 사원내역에 변동사항이 있습니다. 확인 후 다시 시도해 주세요."),
    ALREADYPROCESSED_ERROR("AlreadyProcessedError", "이미 처리된 청구건을 포함하고 있습니다. 확인 후 다시 시도해 주세요."),
    INCLUDERESIGNED_ERROR("IncludeResignedError","퇴사자를 포함하고 있습니다. 확인 후 다시 시도해 주세요.");




    private final String code;
    private final String message;

    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}

package com.douzone.amaranth10.global.interceptor;

import com.douzone.amaranth10.domain.adminmain.application.AdminMainService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@RequiredArgsConstructor
public class GroupManagerCheckInteceptor implements HandlerInterceptor {

    private final AdminMainService adminMainService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession(false);
        String id = (String) session.getAttribute("session"); //세션에 저장된 id

        if(adminMainService.getGroupManagerCheck(id) == null) {
            System.out.println("그룹담당자 권한 없음");
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);  //403 에러
            return false;
        }

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }
}
